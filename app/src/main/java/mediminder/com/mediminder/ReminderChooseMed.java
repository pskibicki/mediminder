package mediminder.com.mediminder;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Basia on 2014-11-16.
 */
public class ReminderChooseMed extends ActionBarActivity implements
        OnClickListener {

    Button button;
    private DatabaseHandler databaseHandler;
    ListView listView;
    ArrayAdapter<String> adapter;
    private ArrayList<MediItem> list;

    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminder_choose_med);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        findViewsById();
        list = getMedList();
        String[] names = new String[list.size()];
        for(int i=0;i<list.size();i++) {
            names[i] = list.get(i).getNazwa();
        }
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_multiple_choice, names);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        listView.setAdapter(adapter);

        button.setOnClickListener(this);
    }

    private void findViewsById() {
        listView = (ListView) findViewById(R.id.medList);
        button = (Button) findViewById(R.id.chooseButton);
    }

    public void onClick(View v) {
        SparseBooleanArray checked = listView.getCheckedItemPositions();
        int[] selectedItems = new int[checked.size()];
        for (int i = 0; i < checked.size(); i++) {
            int position = checked.keyAt(i);
            if (checked.valueAt(i))
                selectedItems[i] = list.get(position).getIdLekuUzytkownika();
            Log.i("fdgfd", String.valueOf(selectedItems[i]));
        }
        if(selectedItems.length > 0) {

            Intent resultIntent = new Intent();
            resultIntent.putExtra("selectedItems", selectedItems);
            setResult(1, resultIntent);
        } else {
            Intent intent = new Intent(getApplicationContext(),
                    ReminderAdd.class);

            setResult(-1, intent);
        }

        finish();
    }

    /**
     * Pobranie listy leków z bazy na podstawie listy id wybranych leków.
     * @return lista leków ze wszystkimi parametrami
     */
    private ArrayList<MediItem> getMedList() {
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            ArrayList<Integer> selectedItems = this.getIntent().getIntegerArrayListExtra("selectedItems");
            return databaseHandler.getSelectedUserMedicines(selectedItems, false);
        }
        catch(Exception e){
            System.out.println("Blad przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd pobierania danych z bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return new ArrayList<MediItem>();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
