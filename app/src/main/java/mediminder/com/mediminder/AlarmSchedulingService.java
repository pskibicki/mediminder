package mediminder.com.mediminder;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import mediminder.com.mediminder.dialogs.EndMedDialog;
import mediminder.com.mediminder.dialogs.ExpDateActivity;
import mediminder.com.mediminder.dialogs.FoodReminderActivity;
import mediminder.com.mediminder.dialogs.ReminderDialogActivity;


/**
 * @author Mikołaj Antczak
 * @author Barbar Ptak
 *
 * Service ustawiajacy alarmy na przypomnieniach raz na dobe lub po zalogowaniu jesli w danym dniu sie nie ustawily
 */
public class AlarmSchedulingService extends IntentService {

    AlarmManager alarmMgr;
    private DatabaseHandler databaseHandler;
    private String currentStringDate;

    /**
     * Inicjacja Service
     */
    public AlarmSchedulingService() {
        super("AlarmSchedulingService");
    }

    /**
     *Obsluga intencji wywolujacej Service. Ttuaj wykonuja sie wlasciwe operacje i uruchamia sie ladowanie alarmow
     *
     * @param intent
     */
    @Override
    protected void onHandleIntent(Intent intent) {

        currentStringDate = Common.currentStringDate();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date currentDate = dateFormat.parse(currentStringDate);
//            currentDate = Common.addDays(currentDate, 1);
            currentStringDate = dateFormat.format(currentDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastScheduling", currentStringDate);
        editor.apply();
        currentStringDate = Common.currentStringDate();
        ArrayList<ReminderItem> remindersList = getCurrentUserReminders(currentStringDate);

        alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        for(ReminderItem item : remindersList)
            if(!Common.checkIfReminderIsAllConfirmed(item))
                addAlarmforReminder(item);
        addExpDateAlarms();
//        Intent intent1 = new Intent(this, DialogActivity.class);
//        intent1.putExtra("date","2015-01-17");
//        intent1.putExtra("time", "20:00");
//        intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        startActivity(intent1);

    }

    /**
     * Ustawia raz na dobe alarmy do przypomnien zazycia lekow
     * @param item
     */
    public void addAlarmforReminder(ReminderItem item) {
        System.out.println(item.getTime());

        Intent intent = new Intent(getApplicationContext(), ReminderDialogActivity.class);
        intent.putExtra("userReminderId", item.getReminderId());
        PendingIntent  alarmIntent = PendingIntent.getActivity(getApplicationContext(), item.getReminderId(), intent, 0);

        boolean zal0 = false;
        boolean zal1 = false;
        boolean zal2 = false;

        for(PillItem pillItem : item.getPills()) {
            ArrayList<Integer> idList = new ArrayList<>();
            idList.add(pillItem.getUserMedicineId());
            ArrayList<MediItem> medicineList = new ArrayList<>();
            try {
                databaseHandler = new DatabaseHandler(getApplicationContext());
                databaseHandler.open();
                medicineList = databaseHandler.getSelectedUserMedicines(idList, true);
            }
            catch(Exception e){
                System.out.println("Błąd przy otwieraniu bazy");
                Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
            }
            finally {
                databaseHandler.close();
            }

            int zalecenieZywieniowe = medicineList.get(0).getWybraneZalecenieZywieniowe();
            if((!zal0 && 0 == zalecenieZywieniowe) || (!zal1 && 1 == zalecenieZywieniowe) || (!zal2 && 2 == zalecenieZywieniowe)) {
                Intent foodIntent = new Intent(getApplicationContext(), FoodReminderActivity.class);
                foodIntent.putExtra("foodId", zalecenieZywieniowe);
                PendingIntent foodAlarmIntent = PendingIntent.getActivity(getApplicationContext(), item.getReminderId(), foodIntent, 0);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd" + "-" + "HH:mm", Locale.getDefault());
                Date itemDate = new Date();
                try {
                    itemDate = dateFormat.parse(currentStringDate + "-" + item.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTime(itemDate);
                switch (medicineList.get(0).getWybraneZalecenieZywieniowe()) {
                    case 0:
                        calendar.add(Calendar.MINUTE, -30);
                        break;
                    case 1:
                        calendar.add(Calendar.MINUTE, -30);
                        break;
                    case 2:
                        calendar.add(Calendar.MINUTE, -30);
                        break;
                }
                calendar.set(Calendar.SECOND, 0);

                if (Build.VERSION.SDK_INT != 19)
                    alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), foodAlarmIntent);
                else
                    alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), foodAlarmIntent);

                if(zalecenieZywieniowe == 0)
                    zal0 = true;
                else if(zalecenieZywieniowe == 1)
                    zal1 = true;
                else if(zalecenieZywieniowe == 2)
                    zal2 = true;
            }
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd" + "-" + "HH:mm", Locale.getDefault());
        Date itemDate = new Date();
        try {
            itemDate = dateFormat.parse(currentStringDate + "-" + item.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTime(itemDate);
        calendar.set(Calendar.SECOND, 0);

        if(Build.VERSION.SDK_INT < 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
    }
    /**
     * Ustawia raz na dobe alarmy o konczej sie dacie waznosci lekow
     *
     */
    private void addExpDateAlarms() {
            ArrayList<String> expMedsList = new ArrayList<>();
            ArrayList<MediItem> medicineList = new ArrayList<>();
            try {
                databaseHandler = new DatabaseHandler(getApplicationContext());
                databaseHandler.open();
                SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                medicineList = databaseHandler.getAllUsersMedicines(sharedPreferences.getInt("id", 1));
            }
            catch(Exception e){
                System.out.println("Błąd przy otwieraniu bazy");
                Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
            }
            finally {
                databaseHandler.close();
            }
        for(MediItem item : medicineList){
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = new Date();
            String currentTime = format.format(date);

            Date current;
            Date expDate;
            try {
                current = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentTime);
                expDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(item.getDataWaznosci());
                int daysApart= (int)((expDate.getTime()-current.getTime())/(1000*60*60*24l));
                if(Math.abs(daysApart)<=3) {
                    expMedsList.add(item.getNazwa());
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
            Intent expIntent = new Intent(getApplicationContext(), ExpDateActivity.class);
            expIntent.putExtra("expList", expMedsList);
            PendingIntent  expAlarmIntent = PendingIntent.getActivity(getApplicationContext(), 1, expIntent, 0);

            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd" + "-" + "HH:mm", Locale.getDefault());
            Date itemDate = new Date();
            try {
                itemDate = dateFormat.parse(currentStringDate + "-" + "23:53");
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar calendar = Calendar.getInstance(Locale.getDefault());
            calendar.setTime(itemDate);

            calendar.set(Calendar.SECOND, 0);

        System.out.println(expMedsList.size() + "||" + System.currentTimeMillis() +"||"+ calendar.getTimeInMillis());

            if(Build.VERSION.SDK_INT != 19)
                alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), expAlarmIntent);
            else
                alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), expAlarmIntent);


    }

    /**
     * Pobiera z bazy liste przypomnien uzytkownika na dany dzien
     *
     * @param currentDate data dla ktorej pobieramy przypomnienia
     * @return liste przypomnien uzytkownika
    */
    private ArrayList<ReminderItem> getCurrentUserReminders(String currentDate) {
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            return databaseHandler.getUserReminders(sharedPreferences.getInt("id", 1), currentDate, currentDate, currentDate);
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd pobierania danych z bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return new ArrayList<ReminderItem>();
    }
}
