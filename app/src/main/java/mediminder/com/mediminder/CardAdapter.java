package mediminder.com.mediminder;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mediminder.com.mediminder.dialogs.ReminderDeleteDialog;
import mediminder.com.mediminder.dialogs.ReminderNoteShowDialog;

/**
 * Created by Mikołaj on 2014-10-07.
 *
 * Obsluguje widok przypomnienia - kartę
 */
public class CardAdapter extends ArrayAdapter<ReminderItem> {

    DatabaseHandler databaseHandler;
    private LayoutInflater mInflater;
    private int layoutGroupResource;
    private ReminderListPage fragment;
    private ValueAnimator alphaAnimator;

    /**
     *
     * Inicjacja Adaptera
     *
     * @param fragment fragment, ktory wyswietla liste prypomnien
     * @param resource ID layoutu larty
     * @param data lista przypomnien
     */
    public CardAdapter(Fragment fragment, int resource, ArrayList<ReminderItem> data) {
        super(fragment.getActivity().getBaseContext(), resource, data);
        this.fragment = (ReminderListPage) fragment;
        this.layoutGroupResource = resource;
        mInflater = (LayoutInflater) fragment.getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    /**
     * Uaktualnia widok listy
     * Dodaje instrukcje jesli jest pusta
     */
    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        fragment.getInfoLayout().setVisibility(getCount() > 0 ? View.GONE : View.VISIBLE);
    }

    /**
     *
     * Kontrola widoku elementu listy - ladownaie danych do widokow i specyfikacja zachowan
     *
     * @param position pozycja obslugiwanego elementu
     * @param convertView - aktualny widok karty
     * @param parent - Widok listy
     * @return zwraca Widok karty po jej przetworzeniu
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(layoutGroupResource, null);
            holder.reminderLayout = (RelativeLayout)convertView.findViewById(R.id.reminder);
            holder.expandedLayout = (RelativeLayout) convertView.findViewById(R.id.expanded_layout);
            holder.hourTextView = (TextView) convertView.findViewById(R.id.card_time);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.card_title);
            holder.pillsListView = (ListView) convertView.findViewById(R.id.pills_list_view);
            holder.buttonLayout = (RelativeLayout) convertView.findViewById(R.id.button_layout);
            holder.confirmButton = (Button) convertView.findViewById(R.id.confirm_button);
            holder.optionButton = (ImageButton) convertView.findViewById(R.id.edit_button);
            holder.separator = convertView.findViewById(R.id.separator);

            holder.reminderLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int getPosition = (Integer) v.getTag();
                    changeViewHeight(getPosition);
                }
            });

            holder.optionButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    fragment.getActivity().registerForContextMenu(fragment.getCardList());
                    v.showContextMenu();
                    fragment.getActivity().unregisterForContextMenu(fragment.getCardList());
                }
            });

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        //tutaj nadajemy wartości dla danego elementu, bez wyjątku.
        // jeżeli jest warunek to musi byc Else - patrz funcja "setPillItemConfirmed", która teoretycznie nic nie zmienia w przypakdu Else

        ReminderItem currentReminder = getItem(position);

        holder.hourTextView.setText(currentReminder.getTime());

        ArrayList<PillItem> pillItems = currentReminder.getPills();

        holder.reminderLayout.setTag(position);
        holder.optionButton.setTag(100);

        String pageDate = fragment.getStartDate();

        ReminderPilllsAdapter reminderPilllsAdapter = new ReminderPilllsAdapter(fragment.getActivity(), R.layout.pills_list_item,
                this, pillItems, holder.confirmButton, currentReminder, pageDate);

        holder.pillsListView.setAdapter(reminderPilllsAdapter);

        ViewGroup.LayoutParams params = holder.pillsListView.getLayoutParams();
        params.height = dpToPx(48f)*holder.pillsListView.getCount() + holder.pillsListView.getDividerHeight()*(holder.pillsListView.getCount()-1);
        holder.pillsListView.setLayoutParams(params);

        holder.titleTextView.setText("Leki (" + reminderPilllsAdapter.getCount() + ")");

        currentReminder.setExpandedHeight(dpToPx(78.5f) + holder.separator.getLayoutParams().height + holder.pillsListView.getLayoutParams().height);

        holder.expandedLayout.getLayoutParams().height = currentReminder.isExpanded() ? currentReminder.getExpandedHeight() : 0;
        holder.expandedLayout.requestLayout();

        setPillItemConfirmed(currentReminder, holder, reminderPilllsAdapter);

        currentReminder.setHolder(holder);

        return convertView;
    }

    /**
     * Zarzadza animacja rozsuwania karty
     *
     * @param getPosition Pozycja przypomnienia na liscie
     */
    private void changeViewHeight(int getPosition) {
        final ReminderItem clickedItem = getItem(getPosition);

        if(clickedItem.isExpanded()){
            collapseOrExpandView(clickedItem, getPosition, clickedItem.getHolder().expandedLayout, clickedItem.getExpandedHeight(), 0);
        } else{
            for(int i = 0; i < getCount(); i++) {
                final ReminderItem item = getItem(i);
                if(item.isExpanded()){
                    collapseOrExpandView(item, i, item.getHolder().expandedLayout, item.getExpandedHeight(), 0);
                }
                else if(i == getPosition){
                    collapseOrExpandView(clickedItem, getPosition, clickedItem.getHolder().expandedLayout, 0, clickedItem.getExpandedHeight());
                }
            }
        }
        notifyDataSetChanged();

    }

    /**
     * Zarzadza animacja rozsuwania/zsuwania karty
     *
     * @param item przypomnienie
     * @param position pozycja na liscie
     * @param changedView zmieniany widok
     * @param fromHeight poczatkowa wysokosc karty
     * @param toHeight koncowa wysokosc karty
     */
    private void collapseOrExpandView(final ReminderItem item, final int position, final View changedView, int fromHeight, int toHeight) {
        alphaAnimator = ValueAnimator.ofInt(fromHeight, toHeight);
        alphaAnimator.setDuration(300);
        alphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animation) {
                Integer value = (Integer) animation.getAnimatedValue();
                changedView.getLayoutParams().height = value;
                changedView.requestLayout();
            }
        });
        alphaAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                item.setExpanded(!item.isExpanded());

                if(item.isExpanded()){
                    ListView listView = fragment.getCardList();
                    try {
                        listView.smoothScrollToPosition(position);
                    } catch (Exception e) {
                        listView.setSelection(position);
                    }
                } else{
                    for(PillItem pillItem : item.getPills()){
                        if(!pillItem.isConfirmed())
                            pillItem.setSelected(false);
                    }
                    ((ReminderPilllsAdapter) item.getHolder().pillsListView.getAdapter()).notifyDataSetChanged();
                }

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        alphaAnimator.start();
    }

    /**
     * Zarzadza animacja alpha po potwierdzeniu zazycia leku
     *
     * @param currentReminder przypomnienie
     * @param holder obiekt klasy trzymajacej informacje o podwidokach karty
     * @param reminderPilllsAdapter Adapter listy lekow karty przypomnienia
     */
    private void setPillItemConfirmed(ReminderItem currentReminder, ViewHolder holder, ReminderPilllsAdapter reminderPilllsAdapter) {
        if(Common.checkIfReminderIsAllConfirmed(currentReminder)){
            if(currentReminder.isAfterAnimation()){
                holder.reminderLayout.setBackgroundColor(getContext().getResources().getColor(R.color.card_disabled));
                holder.reminderLayout.setAlpha(0.5f);
            } else {
                changeAlphaAnimation(holder);
                currentReminder.setAfterAnimation(true);
            }

            holder.confirmButton.setEnabled(false);
        } else{
            holder.reminderLayout.setBackgroundColor(getContext().getResources().getColor(R.color.cardview_light_background));
            holder.reminderLayout.setAlpha(1.0f);
            if(reminderPilllsAdapter.checkIfIsAnyCheckBoxToConfirm())
                holder.confirmButton.setEnabled(true);
            else
                holder.confirmButton.setEnabled(false);
        }
    }

    /**
     * Wykonuje animacje alhpa na karcie przypomnienia
     *
     * @param holder obiekt klasy trzymajacej informacje o podwidokach karty
     */
    private void changeAlphaAnimation(final ViewHolder holder) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.setDuration(300);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        Integer colorFrom = getContext().getResources().getColor(R.color.cardview_light_background);
        Integer colorTo = getContext().getResources().getColor(R.color.card_disabled);
        ValueAnimator colorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                holder.reminderLayout.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        alphaAnimator = ValueAnimator.ofFloat(1.0f, 0.5f);
        alphaAnimator.setDuration(300);
        alphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animator) {
                Float value = (Float) animator.getAnimatedValue();
                holder.reminderLayout.setAlpha(value.floatValue());
            }
        });

        animatorSet.playTogether(colorAnimator, alphaAnimator);
        animatorSet.start();
    }

    /**
     *
     * Konwertuje wymiar widoku z DP do PX
     *
     * @param dp wymiar wyrazony w DP
     * @return Liczba pikseli
     */
    private int dpToPx(float dp) {
        float scale = getContext().getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }

    /**
     * Usuwa przypomnienie z bazy, informuje o tym uzytkownika, wyzwaala usuwanie z widoku jesli jest to aktualna strona
     *
     * @param position Aktualna pozycja przypomnienia
     * @return true - jeślu poprawnie usunieto przypomnienie z bazy. w przeciwnym przypadku - false
     */
    public boolean usunPrzypomnienie(int position) {

        boolean deleted = false;
        try {
            databaseHandler = new DatabaseHandler(getContext());
            databaseHandler.open();
            int id = getItem(position).getReminderId();
            deleted = databaseHandler.deleteReminder(id);
            if(deleted){
                ((Home) fragment.getActivity()).deleteReminderIfExistOnActualPages(id);
            }
        }
        catch(Exception e){
            e.printStackTrace();
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getContext(), "Błąd podczas usuwania leku", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return deleted;
    }


    /**
     * Wyswietla okno dialogowe z notatka przypomnienia
     *
     * @param position Aktualna pozycja przypomnienia
     */
    public void showNote(int position) {
        ReminderItem item = getItem(position);
        //TODO: treść notatki w konstruktorze okna dialogowego notatki
        ReminderNoteShowDialog noteShowDialog = ReminderNoteShowDialog.newInstance(item.getNote());
        noteShowDialog.show(fragment.getActivity().getFragmentManager(), "notatka");
    }

    /**
     * Wyswietla okno edycji przypomnienia
     *
     * @param position Aktualna pozycja przypomnienia
     */
    public void editReminder(int position){
        // TODO: Implement this method
    }

    /**
     * Wyswietla Dialog potwierdzajacy usuwanie przypomnienia
     *
     * @param position Aktualna pozycja przypomnienia
     */
    public void deleteReminder(int position){
        ReminderDeleteDialog newFragment = new ReminderDeleteDialog();
        newFragment.setMediListAndSelectedItemPosition(CardAdapter.this, position);
        newFragment.show(fragment.getActivity().getFragmentManager(), "delete");
    }

    /**
     * Usuwa z widoku karty usuniete z bazy leki uzytkownika
     *
     * @param deletedIDs lista usunietych lekow
     */
    public void deletePillsFromReminders(ArrayList<Integer> deletedIDs) {
        ArrayList<ReminderItem> emptyReminders = new ArrayList<>();

        for(int i = 0; i < getCount(); i++){
            ReminderItem reminder = getItem(i);
            ArrayList<PillItem> pillItems = reminder.getPills();
            for(PillItem pillItem : pillItems){
                if(deletedIDs.contains(pillItem.getUserMedicineId()))
                    pillItems.remove(pillItem);
            }
            if(pillItems.isEmpty())
                emptyReminders.add(reminder);
        }

        for(ReminderItem item : emptyReminders)
            remove(item);

        emptyReminders = null;

        notifyDataSetChanged();
    }

    /**
     * Trzyma informacje o aktualnym stanie widoków karty.
     */
    public static class ViewHolder {

        public RelativeLayout reminderLayout;
        public RelativeLayout expandedLayout;
        public TextView hourTextView;
        public TextView titleTextView;
        public ListView pillsListView;
        public RelativeLayout buttonLayout;
        public Button confirmButton;
        public ImageButton optionButton;
        public View separator;
    }
}
