package mediminder.com.mediminder;

import android.content.Context;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MediAdapter extends ArrayAdapter<MediItem> {

    private final LayoutInflater mInflater;
    private final SparseArray<String> rodzajeJednostek;
    private int layoutResource;

    public MediAdapter(Context context, int resource, ArrayList<MediItem> objects) {
        super(context, resource, objects);
        this.layoutResource = resource;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rodzajeJednostek = new SparseArray<>();
        rodzajeJednostek.append(R.drawable.tab_zwykla, "szt");
        rodzajeJednostek.append(R.drawable.tab_dluga2,"szt" );
        rodzajeJednostek.append(R.drawable.tab_dzielona,"szt" );
        rodzajeJednostek.append(R.drawable.tab_dluga1,"szt" );
        rodzajeJednostek.append(R.drawable.tab_syrop,"ml");
        rodzajeJednostek.append(R.drawable.tab_strzykawka,"ml" );
        rodzajeJednostek.append(R.drawable.tab_dluga3,"szt" );
        rodzajeJednostek.append(R.drawable.tab_kropla,"szt" );
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;
//        int rowType = getItemViewType(position);

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(layoutResource, null);
            holder.rodzajleku = (ImageView) convertView.findViewById(R.id.rodzaj_leku);
            holder.nazwaleku = (TextView) convertView.findViewById(R.id.nazwa_leku);
            holder.aktualnaIlosc = (TextView) convertView.findViewById(R.id.aktualna_ilosc_leku);
            holder.jednostka = (TextView) convertView.findViewById(R.id.jednostka);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MediItem lek = getItem(position);
        holder.nazwaleku.setText(lek.getNazwa());
        holder.rodzajleku.setImageDrawable(getContext().getResources().getDrawable(lek.getRodzaj()));
        holder.aktualnaIlosc.setText(String.valueOf(lek.getAktualnaIloscSztuk()));
        holder.jednostka.setText(rodzajeJednostek.get(lek.getRodzaj()));

        return convertView;
    }

    public static class ViewHolder {
        public ImageView rodzajleku;
        public TextView nazwaleku;
        public TextView aktualnaIlosc;
        public TextView jednostka;
    }

}
