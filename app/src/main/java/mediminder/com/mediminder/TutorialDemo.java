package mediminder.com.mediminder;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;


public class TutorialDemo extends ActionBarActivity {

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tutorial_demo);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        ViewPager pager = (ViewPager) findViewById(R.id.tutorial_demo);
        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), this);
        pager.setAdapter(adapter);
        pager.setPageTransformer(true, new DepthPageTransformer());

    }

    @Override
    public void onBackPressed(){

    }

    public static class PagerAdapter extends FragmentPagerAdapter{

        ArrayList<String> pageImagesName;
        /**
         * Dostosowanie grafik tutoriala do ekranu urządzenia
         */
        public PagerAdapter(FragmentManager fm, Activity activity) {
            super(fm);

            int dpiDensity = activity.getResources().getDisplayMetrics().densityDpi;
            switch(dpiDensity){
                case DisplayMetrics.DENSITY_HIGH:
                    //Do sth for LDPI-screen devices
                    pageImagesName = new ArrayList<>();
                    pageImagesName.add("tutodemo0_sdpi.png");
                    pageImagesName.add("tutodemo1_sdpi.png");
                    pageImagesName.add("tutodemo2_sdpi.png");
                    pageImagesName.add("tutodemo3_sdpi.png");
                    pageImagesName.add("tutodemo4_sdpi.png");
                    pageImagesName.add("tutodemo5_sdpi.png");
                    pageImagesName.add("tutodemo6_sdpi.png");
                    pageImagesName.add("tutodemo7_sdpi.png");
                    break;
                case DisplayMetrics.DENSITY_XHIGH:
                    //Do sth for MDPI-screen devices
                    pageImagesName = new ArrayList<>();
                    pageImagesName.add("tutodemo0_mdpi.png");
                    pageImagesName.add("tutodemo1_mdpi.png");
                    pageImagesName.add("tutodemo2_mdpi.png");
                    pageImagesName.add("tutodemo3_mdpi.png");
                    pageImagesName.add("tutodemo4_mdpi.png");
                    pageImagesName.add("tutodemo5_mdpi.png");
                    pageImagesName.add("tutodemo6_mdpi.png");
                    pageImagesName.add("tutodemo7_mdpi.png");
                    break;
                case DisplayMetrics.DENSITY_XXHIGH:
                    //Do sth for HDPI-screen devices
                    pageImagesName = new ArrayList<>();
                    pageImagesName.add("tutodemo0_hdpi.png");
                    pageImagesName.add("tutodemo1_hdpi.png");
                    pageImagesName.add("tutodemo2_hdpi.png");
                    pageImagesName.add("tutodemo3_hdpi.png");
                    pageImagesName.add("tutodemo4_hdpi.png");
                    pageImagesName.add("tutodemo5_hdpi.png");
                    pageImagesName.add("tutodemo6_hdpi.png");
                    pageImagesName.add("tutodemo7_hdpi.png");
                    break;
            }
        }

        @Override
        public Fragment getItem(int position) {
            TutorialPage page = TutorialPage.newInstance(pageImagesName.get(position));
            return page;
        }

        @Override
        public int getCount() {
            return pageImagesName.size();
        }
    }

    public static class TutorialPage extends Fragment{

        public static final String DRAWABLE = "image_drawable";

        private String imageDrawableName;

        public static TutorialPage newInstance(String imageDrawableId) {
            TutorialPage fragment = new TutorialPage();
            Bundle args = new Bundle();
            args.putString(DRAWABLE, imageDrawableId);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            imageDrawableName = getArguments().getString(DRAWABLE);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.tutorial_page, container, false);

            ImageView tutorialImage = (ImageView) rootView.findViewById(R.id.tutorial_page_image);
            AssetManager assets = getResources().getAssets();
            InputStream buffer = null;
            try {
                buffer = new BufferedInputStream((assets.open(imageDrawableName)));
                tutorialImage.setImageBitmap(BitmapFactory.decodeStream(buffer));
                buffer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(imageDrawableName.startsWith("tutodemo7")){
                Button button = (Button) rootView.findViewById(R.id.end_button);
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(TutorialPage.this.getActivity(), Home.class);
                        startActivity(intent);
                        TutorialPage.this.getActivity().finish();
                    }
                });
            }

            return rootView;
        }
    }

    public class DepthPageTransformer implements ViewPager.PageTransformer {
        private static final float MIN_SCALE = 0.75f;

        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.setAlpha(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.setAlpha(1);
                view.setTranslationX(0);
                view.setScaleX(1);
                view.setScaleY(1);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.setAlpha(1 - position);

                // Counteract the default slide transition
                view.setTranslationX(pageWidth * -position);

                // Scale the page down (between MIN_SCALE and 1)
                float scaleFactor = MIN_SCALE
                        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                view.setScaleX(scaleFactor);
                view.setScaleY(scaleFactor);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.setAlpha(0);
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}