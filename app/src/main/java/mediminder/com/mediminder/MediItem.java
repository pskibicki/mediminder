package mediminder.com.mediminder;

/**
 * Created by Marcin on 2014-11-04.
 */
public class MediItem {

    private int idLekuUzytkownika;
    private int mediID;
    private String nazwa;
    private int dawkaWJednejSztuce;
    private int rodzaj;
    private int kolor;
    private int iloscOpakowan;
    private int iloscSztukWOpakowaniu;
    private int aktualnaIloscSztuk;
    private String dataWaznosci;
    private int priorytet;
    private String notatka;
    private int wybraneZalecenieZywieniowe;

    public MediItem(int idLekuUzytkownika, String nazwa, int rodzaj, int kolor, int aktualnaIloscSztuk) {
        this.idLekuUzytkownika = idLekuUzytkownika;
        this.nazwa = nazwa;
        this.rodzaj = rodzaj;
        this.kolor = kolor;
        this.aktualnaIloscSztuk = aktualnaIloscSztuk;

    }

    public MediItem(int idLekuUzytkownika, String nazwa, int dawka, int rodzaj, int kolor, int aktualnaIloscSztuk, String dataWaznosci) {
        this.idLekuUzytkownika = idLekuUzytkownika;
        this.nazwa = nazwa;
        this.dawkaWJednejSztuce = dawka;
        this.rodzaj = rodzaj;
        this.kolor = kolor;
        this.aktualnaIloscSztuk = aktualnaIloscSztuk;
        this.dataWaznosci = dataWaznosci;
    }

    public MediItem(int idLekuUzytkownika, String nazwa, int rodzaj, int kolor, int aktualnaIloscSztuk,
                    int iloscOpakowan, int iloscSztukWOpakowaniu, String dataWaznosci, int priorytet, String notatka, int wybraneZalecenieZywieniowe){
        this. idLekuUzytkownika = idLekuUzytkownika;
        this.nazwa = nazwa;
        this.rodzaj = rodzaj;
        this.kolor = kolor;
        this.iloscOpakowan = iloscOpakowan;
        this.iloscSztukWOpakowaniu = iloscSztukWOpakowaniu;
        this.aktualnaIloscSztuk = aktualnaIloscSztuk;
        this.dataWaznosci = dataWaznosci;
        this.priorytet = priorytet;
        this.notatka = notatka;
        this.wybraneZalecenieZywieniowe = wybraneZalecenieZywieniowe;
    }

    public int getIdLekuUzytkownika() {
        return idLekuUzytkownika;
    }

    public void setIdLekuUzytkownika(int idLekuUzytkownika) {
        this.idLekuUzytkownika = idLekuUzytkownika;
    }

    public String getNazwa() {
        return nazwa;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public int getRodzaj() {
        return rodzaj;
    }

    public void setRodzaj(int rodzaj) {
        this.rodzaj = rodzaj;
    }

    public int getKolor() {
        return kolor;
    }

    public void setKolor(int kolor) {
        this.kolor = kolor;
    }

    public int getMediID() {
        return mediID;
    }

    public void setMediID(int mediID) {
        this.mediID = mediID;
    }

    public int getDawkaWJednejSztuce() {
        return dawkaWJednejSztuce;
    }

    public void setDawkaWJednejSztuce(int dawkaWJednejSztuce) {
        this.dawkaWJednejSztuce = dawkaWJednejSztuce;
    }

    public int getIloscOpakowan() {
        return iloscOpakowan;
    }

    public void setIloscOpakowan(int iloscOpakowan) {
        this.iloscOpakowan = iloscOpakowan;
    }

    public int getIloscSztukWOpakowaniu() {
        return iloscSztukWOpakowaniu;
    }

    public void setIloscSztukWOpakowaniu(int iloscSztukWOpakowaniu) {
        this.iloscSztukWOpakowaniu = iloscSztukWOpakowaniu;
    }

    public int getAktualnaIloscSztuk() {
        return aktualnaIloscSztuk;
    }

    public void setAktualnaIloscSztuk(int aktualnaIloscSztuk) {
        this.aktualnaIloscSztuk = aktualnaIloscSztuk;
    }

    public String getDataWaznosci() {
        return dataWaznosci;
    }

    public void setDataWaznosci(String dataWaznosci) {
        this.dataWaznosci = dataWaznosci;
    }

    public int getPriorytet() {
        return priorytet;
    }

    public void setPriorytet(int priorytet) {
        this.priorytet = priorytet;
    }

    public String getNotatka() {
        return notatka;
    }

    public void setNotatka(String notatka) {
        this.notatka = notatka;
    }

    public int getWybraneZalecenieZywieniowe() {
        return wybraneZalecenieZywieniowe;
    }

    public void setWybraneZalecenieZywieniowe(int wybraneZalecenieZywieniowe) {
        this.wybraneZalecenieZywieniowe = wybraneZalecenieZywieniowe;
    }
}
