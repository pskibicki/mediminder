package mediminder.com.mediminder;

/**
 *@author Mikołaj Antczak
 */

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Home extends ActionBarActivity {

    /**
     *     Layout gornego paska z data
     */
    private RelativeLayout dateInfo;
    /**
     * kontrolka wyswietlajaca dzien
     */
    private TextView scopeTextButton;
    /**
     * expandButton przycisk (strzalka) wyswietlajacy kalendarz
     */
    private ImageButton expandButton;
    /**
     * Wyswietlanie kalendarza
     */
    private GridView scopeSelector;
    /**
     * Przyciemnienie ekranu kiedy wyswietlony kalendarz
     */
    private View displayCover;

    /**
     * Glowny widok odpowiedzialny za wyswietlanie stron
     */
    private ViewPager mPager;
    /**
     * Adapter odpowiedzialny za ladowanie odpowiedniej strony
     */
    private ReminderPagerAdapter reminderPagerAdapter;
    /**
     * tryb wyswietlania: 0 = dzienny, 1 = tygodniowy
     */
    private int mode = 0;

    /**
     * nazwy miesiecy
     */
    private String[] monthsName;
    /**
     * Nazwy dni
     */
    private String[] daysOfWeek;

    /**
     *
     */
    private BroadcastReceiver broadcastReceiver;

    /**
     *
     * Buduje aktywnosc po uruchomieniu
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        setDailyAlarmsIfNotExtist();

        monthsName = new String[12];
        monthsName[0] = "stycznia";
        monthsName[1] = "lutego";
        monthsName[2] = "marca";
        monthsName[3] = "kwietnia";
        monthsName[4] = "maja";
        monthsName[5] = "czerwca";
        monthsName[6] = "lipca";
        monthsName[7] = "sierpnia";
        monthsName[8] = "września";
        monthsName[9] = "października";
        monthsName[10] = "listopada";
        monthsName[11] = "grudnia";

        daysOfWeek = new String[7];
        daysOfWeek[0] = "pn.";
        daysOfWeek[1] = "wt.";
        daysOfWeek[2] = "śr.";
        daysOfWeek[3] = "czw.";
        daysOfWeek[4] = "pt.";
        daysOfWeek[5] = "sob.";
        daysOfWeek[6] = "niedz.";
//        mFab = (FloatingActionButton)findViewById(R.id.fabbutton);

        dateInfo = (RelativeLayout) findViewById(R.id.date_info);
        scopeTextButton = (TextView) findViewById(R.id.scope_text_view);
        scopeSelector = (GridView) findViewById(R.id.scope_selector);
        expandButton = (ImageButton) findViewById(R.id.expand_button);
        displayCover = findViewById(R.id.display_cover);

        expandButton.setVisibility(View.INVISIBLE);
        //initScopeNavigation();

        mPager = (ViewPager) findViewById(R.id.pager);

        reminderPagerAdapter = new ReminderPagerAdapter(getSupportFragmentManager());
        reminderPagerAdapter.setMode(mode);
        mPager.setAdapter(reminderPagerAdapter);
        setPagerListeners();
        mPager.setCurrentItem(getCurrentDatePageNumber(), false);

//        uruchamia nasluchiwanie scrollowania potrzebnego do dzialania plywajacego przycisku
//        mFab.listenTo(currentPageFragment.getListView());

    }

    /**
     * Ustawia alarmy jeśli danego dnia nie zostały jeszcze pobrane
     */
    private void setDailyAlarmsIfNotExtist() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        AlarmReceiver alarmReceiver = new AlarmReceiver();

        Date currentDate = new Date();
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTime(currentDate);
//        calendar.add(Calendar.MINUTE, 30);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String currentDateForScheduling = dateFormat.format(calendar.getTime());
//zakomentowac
        if(preferences.getBoolean("pref_sync", true) && !preferences.getString("lastScheduling", "").equals(currentDateForScheduling))
            alarmReceiver.setDailyAlarms(this);
    }

    /**
     *
     * @return Numer obecnie wyswietlanej strony
     *
     */
    private int getCurrentDatePageNumber() { //wylicza numer dnia na podstawie pierwszego dnia z pagera i obecnego dnia (po prosto days between)
//        long diff = new Date().getTime();
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(System.currentTimeMillis());
        return (int) TimeUnit.DAYS.convert(calendar.getTimeInMillis() + 1000* 3600 , TimeUnit.MILLISECONDS);
    }

    /**
     *
     * Funkcja uruchamia aktywnosc dodawania leku
     *
     * @param view widok, ktorego klikniecie wyzwala funkcje
     */
    public void addReminder(View view) {
        Intent intent = new Intent(getBaseContext(), ReminderAdd.class);
        startActivityForResult(intent,0);
    }

    /**
     *
     * Funkcja wyzwalana podczas powrotu do biezacej aktywnosci z innej, ktora zostala uruchomiona w celu zebrania wynikow
     *
     * @param requestCode kod zadania uruchomienia aktwnosci
     * @param resultCode kod wyniku z aktwnosci wczesniej uruchomionej: 1 = dodanie przypomnienia, 2 - usuniecie leku z listy lekow
     * @param data intencja z ktora zostala uruchomiona biezaca aktywnosc
     */
    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){
        if(resultCode == 1){
            ArrayList<MedChoiceItem> list = data.getParcelableArrayListExtra("key");
            int idPrzypomnienia = data.getIntExtra("ID", -1);
            String time = data.getStringExtra("time");
            String startDate = data.getStringExtra("startDate");
            String stopDate = data.getStringExtra("stopDate");
            String note = data.getStringExtra("note");
            ArrayList<PillItem> pillsList = new ArrayList<>();

            for(MedChoiceItem item : list) {
                pillsList.add(new PillItem(item.getUserMedicineReminderId(), item.getName(), item.getType(), item.getColor(), item.getDosage(), false, item.getUserMedicineId()));
            }
            Iterator it = reminderPagerAdapter.mPageReferenceMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Integer, ReminderListPage> pairs = (Map.Entry)it.next();
                ReminderListPage page = pairs.getValue();

                if(Common.checkIfReminderIsOnSelectedPage(startDate, stopDate, page)){
                    page.getReminderItems().add(new ReminderItem(idPrzypomnienia, startDate, stopDate, time, pillsList, note));
                    Collections.sort(page.getReminderItems(), new ReminderTimeComparator());
                    ((CardAdapter) page.getCardList().getAdapter()).notifyDataSetChanged();
                }

//                it.remove(); // avoids a ConcurrentModificationException
            }
        } else if(resultCode == 2) {
            ArrayList<Integer> deletedIDs = data.getIntegerArrayListExtra("idUsuniete");
            Iterator it = reminderPagerAdapter.mPageReferenceMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry<Integer, ReminderListPage> pairs = (Map.Entry) it.next();
                ReminderListPage page = pairs.getValue();
                CardAdapter adapter = (CardAdapter) page.getCardList().getAdapter();
                adapter.deletePillsFromReminders(deletedIDs);
            }
        }

    }

    /**
     *
     * Usuwa przypomnienie jesli istnieje na aktualnie wyswietlanej stronie
     * Uzywne po usunieciu leku uzytkownika gdy przypomnienie robi sie puste
     *
     * @param reminderId ID przpomnienia
     */
    public void deleteReminderIfExistOnActualPages(int reminderId){
        Iterator it = reminderPagerAdapter.mPageReferenceMap.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry<Integer, ReminderListPage> pairs = (Map.Entry)it.next();
            ReminderListPage page = pairs.getValue();
            ArrayList<ReminderItem> pageReminders = page.getReminderItems();

            int i = 0;
            while(i < pageReminders.size() && reminderId != pageReminders.get(i).getReminderId())
                i++;

            if(i < pageReminders.size() && reminderId == pageReminders.get(i).getReminderId())
                pageReminders.remove(i);

            ((CardAdapter) page.getCardList().getAdapter()).notifyDataSetChanged();
        }
    }

    /**
     *
     * tworzy menu kontekstowe pojawiajace sie po przycisnieciu na przypomnieniu "opcji"
     *
     * @param menu menu kontekstowe
     * @param v widok ktory wywolal menu
     * @param menuInfo informacje o menu (w tym pozycja na liscie z ktorej zostalo wywolane)
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;

        ReminderListPage actualPageFragment = reminderPagerAdapter.getFragment(mPager.getCurrentItem());
        ReminderItem item = ((CardAdapter) actualPageFragment.getCardList().getAdapter()).getItem(info.position);

        menu.add(0, 0, 0, "Notatka").setVisible(item.getNote() != null);
//        menu.add(0, 1, 1, "Edytuj");
        menu.add(0, 2, 2, "Usuń");
    }


    /**
     *
     * Specyfikuje akcje uruchamiane po wyborze w menu kontekstowym
     *
     * @param item wybrana opcja menu
     * @return
     */
        @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        ReminderListPage actualPageFragment = reminderPagerAdapter.getFragment(mPager.getCurrentItem());
        CardAdapter cardAdapter = (CardAdapter) (actualPageFragment.getCardList().getAdapter());
        switch (item.getItemId()){
            case 0: cardAdapter.showNote(info.position); break;
            case 1: cardAdapter.editReminder(info.position); break;
            case 2: cardAdapter.deleteReminder(info.position); break;
        }
        return true;
    }

    /**
     *
     * Tworzy menu na ActionBar
     *
     * @param menu menu na ActionBar
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    /**
     *
     * Specyfikuje akcje uruchamiane po wyborze w menu na ActionBar
     *
     * @param item wybrana opcja menu
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, Settings.class);
            startActivity(settingsIntent);
        } else if(id == R.id.action_medi_list){
            Intent mediListIntent = new Intent(this, MediList.class);
            startActivityForResult(mediListIntent, 1);
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     *
     * Nasłuchuje zmiany aktualnie wyswietlanej strony w ViewPager
     */
    private void setPagerListeners() {
        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                Date date = null;
                if (reminderPagerAdapter.getFragment(position) == null) {
                    date = new Date();
                } else {
                    try {
                        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                        String dateFormatted = reminderPagerAdapter.getFragment(position).getStartDate();
                        date = dateFormat.parse(dateFormatted);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                scopeTextButton.setText(calendar.get(Calendar.DAY_OF_MONTH) + " " + monthsName[calendar.get(Calendar.MONTH)]
                        + " " + calendar.get(Calendar.YEAR) + " (" + daysOfWeek[calendar.get(Calendar.DAY_OF_WEEK) == 1 ? 6 : calendar.get(Calendar.DAY_OF_WEEK)-2] + ")");
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    /**
     * Porownuje przypomnienia pod wzgledem czasu
     */
    public static class ReminderTimeComparator implements Comparator<ReminderItem> {
        @Override
        public int compare(ReminderItem item1, ReminderItem item2) {
            return item1.getTime().compareTo(item2.getTime());
        }
    }

    /**
     * Konczy dzialanie activity
     */
    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }

}