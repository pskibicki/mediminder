package mediminder.com.mediminder;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import mediminder.com.mediminder.dialogs.ExpDateActivity;
import mediminder.com.mediminder.dialogs.NoteDialog;

public class MediAdd extends ActionBarActivity {

    private DatabaseHandler databaseHandler;

    private Button buttonDodaj;
    private EditText edittext1;
    private EditText edittext2;
    private EditText edittext3;
    private EditText edittext4;

    private ImageButton imagebtn1;
    private ImageButton imagebtn2;
    private ImageButton imagebtn3;
    private ImageButton imagebtn4;
    private ImageButton imagebtn5;
    private ImageButton imagebtn6;
    private ImageButton imagebtn7;
    private ImageButton imagebtn8;

    private HashMap<Integer, Integer> rodzajeMap;
    int wybranyRodzaj;

    private ImageButton green_button;
    private ImageButton blue_button;
    private ImageButton red_button;
    private ImageButton yellow_button;
    private ImageButton white_button;
    private ImageButton gray_button;
    private ImageButton violet_button;
    private ImageButton orange_button;
    private ImageButton pink_button;
    private ImageButton brown_button;

    private TextView nazwa_btn;
    private TextView textViewDodajNotatke;
    private ImageButton[] rodzaje;
    private String notatka="";

    private RadioGroup radioGroup;
    private RadioGroup radioGroup2;
    int wybranyPriorytet;
    int wybraneZalecenieZywnosciowe;
    private int idLekuUzytkownika;
    private int poczatkowaLiczbaOpakowan;
    private int aktualnaIlosc;

    private BroadcastReceiver broadcastReceiver;

    /**
     * Inicjacja aktywnosci
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medi_add);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        edittext1 = (EditText) findViewById(R.id.editText1);
        edittext2 = (EditText) findViewById(R.id.editText2);
        edittext3 = (EditText) findViewById(R.id.editText3);
        edittext4 = (EditText) findViewById(R.id.editText4);
        addListenerOnRadioButton();
        addListenerOnRadioButton2();

        wprowadzenieDatyWaznosciLeku();


        nazwa_btn = (TextView) findViewById(R.id.textView1);

        textViewDodajNotatke = (TextView) findViewById(R.id.textViewDodajNotatke);
        wprowadzenieNotatki();

        imagebtn1 = (ImageButton) findViewById(R.id.imagebtn1);
        imagebtn2 = (ImageButton) findViewById(R.id.imagebtn2);
        imagebtn3 = (ImageButton) findViewById(R.id.imagebtn3);
        imagebtn4 = (ImageButton) findViewById(R.id.imagebtn4);
        imagebtn5 = (ImageButton) findViewById(R.id.imagebtn5);
        imagebtn6 = (ImageButton) findViewById(R.id.imagebtn6);
        imagebtn7 = (ImageButton) findViewById(R.id.imagebtn7);
        imagebtn8 = (ImageButton) findViewById(R.id.imagebtn8);

        rodzaje = new ImageButton[8];
        rodzaje[0] = imagebtn1;
        rodzaje[1] = imagebtn2;
        rodzaje[2] = imagebtn3;
        rodzaje[3] = imagebtn4;
        rodzaje[4] = imagebtn5;
        rodzaje[5] = imagebtn6;
        rodzaje[6] = imagebtn7;
        rodzaje[7] = imagebtn8;

        rodzajeMap = new HashMap<Integer, Integer>();
        rodzajeMap.put(imagebtn1.getId(), R.drawable.tab_zwykla);
        rodzajeMap.put(imagebtn2.getId(), R.drawable.tab_dluga2);
        rodzajeMap.put(imagebtn3.getId(), R.drawable.tab_dzielona);
        rodzajeMap.put(imagebtn4.getId(), R.drawable.tab_dluga1);
        rodzajeMap.put(imagebtn5.getId(), R.drawable.tab_syrop);
        rodzajeMap.put(imagebtn6.getId(), R.drawable.tab_strzykawka);
        rodzajeMap.put(imagebtn7.getId(), R.drawable.tab_dluga3);
        rodzajeMap.put(imagebtn8.getId(), R.drawable.tab_kropla);

        green_button = (ImageButton) findViewById(R.id.color1button);
        blue_button = (ImageButton) findViewById(R.id.color2button);
        red_button = (ImageButton) findViewById(R.id.color3button);
        yellow_button = (ImageButton) findViewById(R.id.color4button);
        white_button = (ImageButton) findViewById(R.id.color5button);
        gray_button = (ImageButton) findViewById(R.id.color6button);
        violet_button = (ImageButton) findViewById(R.id.color7button);
        orange_button = (ImageButton) findViewById(R.id.color8button);
        pink_button = (ImageButton) findViewById(R.id.color9button);
        brown_button = (ImageButton) findViewById(R.id.color10button);

        imagebtn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        imagebtn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha(v);
            }
        });
        green_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        blue_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        red_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        yellow_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        white_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        gray_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        violet_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        orange_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        pink_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });
        brown_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeAlpha2(v);
            }
        });

        buttonDodaj = (Button) findViewById(R.id.button_dodaj);

        inicjacjaPrzyciskuDodawania();

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            if((idLekuUzytkownika = extras.getInt("idLekuUzytkownika")) != 0) {
                getSupportActionBar().setTitle("Edytuj lek");
                ArrayList<Integer> idList = new ArrayList<>();
                idList.add(idLekuUzytkownika);
                ArrayList<MediItem> listaLekow = new ArrayList<>();

                try {
                    databaseHandler = new DatabaseHandler(getApplicationContext());
                    databaseHandler.open();
                    listaLekow = databaseHandler.getSelectedUserMedicines(idList, true);
                }
                catch(Exception e){
                    System.out.println("Błąd przy otwieraniu bazy");
                    Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
                }
                finally {
                    databaseHandler.close();
                }

                MediItem item = listaLekow.get(0);
                edittext1.setText(item.getNazwa());
                poczatkowaLiczbaOpakowan = item.getIloscOpakowan();
                System.out.println(item.getAktualnaIloscSztuk());
                aktualnaIlosc = item.getAktualnaIloscSztuk();
                System.out.println(aktualnaIlosc);
                edittext2.setText(String.valueOf(poczatkowaLiczbaOpakowan));
                edittext3.setText(String.valueOf(item.getIloscSztukWOpakowaniu()));
                edittext4.setText(item.getDataWaznosci());
                ((RadioButton) radioGroup.getChildAt(item.getPriorytet())).setChecked(true);
                wybranyPriorytet = item.getPriorytet();
                boolean found = false;
                for(int i = 0; i < rodzaje.length && !found; i++){
                    ImageButton button = rodzaje[i];
                    if(rodzajeMap.get(button.getId()) == item.getRodzaj()){
                        button.setAlpha(1.0f);
                        found = true;
                    }
                }
                ((RadioButton) radioGroup2.getChildAt(item.getWybraneZalecenieZywieniowe())).setChecked(true);
                buttonDodaj.setText("Zatwierdź edycję");

                zablokujEdycje();
                initEditButtonListener(idLekuUzytkownika);
            }
        }
    }

    /**
     * dodawanie leku do bazy
     */
    private void inicjacjaPrzyciskuDodawania() {
        buttonDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nazwaLeku = edittext1.getText().toString();
                String iloscOpakowan = edittext2.getText().toString();
                String iloscSztukWOpak = edittext3.getText().toString();
                String dataWaznosci = edittext4.getText().toString();

                if (iloscOpakowan.equals("0")) {
                    Toast.makeText(getBaseContext(), "Wprowadź ilość posiadanych opakowań", Toast.LENGTH_LONG).show();
                } else {
                    if (iloscSztukWOpak.equals("0"))
                        Toast.makeText(getBaseContext(), "Wprowadź ilość sztuk w opakowaniu", Toast.LENGTH_LONG).show();
                    else {
                        if (nazwaLeku.isEmpty() || wybranyRodzaj == 0 || iloscOpakowan.isEmpty() || iloscSztukWOpak.isEmpty() || dataWaznosci.isEmpty())
                            Toast.makeText(getBaseContext(), "Wprowadź wszystkie dane", Toast.LENGTH_SHORT).show();
                        else {
                            long idLekuUzytkownika = dodajLekDoBazy(nazwaLeku, wybranyRodzaj, Integer.valueOf(iloscOpakowan), Integer.valueOf(iloscSztukWOpak), wybranyPriorytet, notatka, wybraneZalecenieZywnosciowe, dataWaznosci);

                            if (idLekuUzytkownika != -1) {

                                //sprawdzenie czy data waznosci sie konczy
                                DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                                Date date = new Date();
                                String currentTime = format.format(date);
                                Date current;
                                Date expDate;
                                try {
                                    current = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(currentTime);
                                    expDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(dataWaznosci);

                                    int daysApart= (int)((expDate.getTime()-current.getTime())/(1000*60*60*24l));
                                    if(Math.abs(daysApart)<=3)
                                        addExpDateAlarms(nazwaLeku);

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }

                                Intent resultIntent = new Intent();
                                resultIntent.putExtra("ID", (int) idLekuUzytkownika);
                                resultIntent.putExtra("Nazwa", edittext1.getText().toString());
                                resultIntent.putExtra("Rodzaj", wybranyRodzaj);
                                resultIntent.putExtra("Color", 0);
                                resultIntent.putExtra("Ilosc", Integer.valueOf(iloscOpakowan) * Integer.valueOf(iloscSztukWOpak));
                                setResult(1, resultIntent);
                                finish();
                            }
                        }

                    }
                }
            }
        });
    }

    /**
     * Wprowadzenie dodatkowej notatki
     */
    private void wprowadzenieNotatki() {
        textViewDodajNotatke.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteDialog rd = new NoteDialog();
                rd.setMediAdd(MediAdd.this);
                rd.show(getFragmentManager(), "notatka");
            }
        });
    }

    /**
     * Wprowadzenie daty ważności leku
     */
    private void wprowadzenieDatyWaznosciLeku() {
        edittext4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        //tutaj wybrana data. uwaga: miesiące w formacie 0-11
                        monthOfYear = monthOfYear + 1;
                        //TODO: żeby dobrze wuszukiwac potem w bazie datę ważności to nie może być data w formacie np. 2013-11-8 tylko 2013-11-08
                        edittext4.setText(year + "-" + monthOfYear + "-" + dayOfMonth);
                    }
                };

                String currentDate = currentDate();
                int currentYear = Integer.valueOf(currentDate().substring(6));
                int currentMonth = Integer.valueOf(currentDate.substring(3, 5)) - 1;
                int currentDay = Integer.valueOf(currentDate.substring(0, 2));

                DatePickerDialog dpd = new DatePickerDialog(MediAdd.this, onDateSetListener, currentYear, currentMonth, currentDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Date d = null;
                try {
                    d = sdf.parse(currentDate());
                    dpd.getDatePicker().setMinDate(d.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                dpd.show();
            }
        });
    }

    /**
     * dodaje lek
     *
     * @param idLekuUzytkownika id luku użytkownika
     */
    private void initEditButtonListener(final int idLekuUzytkownika) {
        buttonDodaj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String liczbaOpakowan = edittext2.getText().toString();
               if (liczbaOpakowan.equals("0")) {
                    Toast.makeText(getBaseContext(), "Wprowadź ilość posiadanych opakowań", Toast.LENGTH_LONG).show();
               } else if (liczbaOpakowan.isEmpty())
                   Toast.makeText(getBaseContext(), "Wprowadź wszystkie dane", Toast.LENGTH_SHORT).show();
               else {
                   aktualnaIlosc = (Integer.valueOf(liczbaOpakowan) - poczatkowaLiczbaOpakowan)*Integer.valueOf(edittext3.getText().toString()) + aktualnaIlosc;
                   boolean edited = edytujLekWBazie(idLekuUzytkownika, Integer.valueOf(liczbaOpakowan), wybranyPriorytet);
                   if (edited) {
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra("position", getIntent().getIntExtra("position", -1));
                        resultIntent.putExtra("Ilosc", aktualnaIlosc);
                        setResult(2, resultIntent);
                        finish();
                   }
               }
            }
        });
    }
    /**
     * Modyfikowanie leku
     */
    private boolean edytujLekWBazie(int idLekuUzytkownika, int liczbaOpakowan, int wybranyPriorytet) {
        boolean edited = false;
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            edited = databaseHandler.editUserMedicine(idLekuUzytkownika, liczbaOpakowan, aktualnaIlosc, wybranyPriorytet);
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return edited;
    }

    /**
     * blokuje edycje jesli wszystko nie wypelnione
     */
    private void zablokujEdycje() {
        edittext1.setEnabled(false);
        edittext3.setEnabled(false);
        edittext4.setEnabled(false);
        for(ImageButton button : rodzaje)
            button.setEnabled(false);
        for(int i = 0; i < radioGroup2.getChildCount(); i++){
            ((RadioButton)radioGroup2.getChildAt(i)).setEnabled(false);
        }
        textViewDodajNotatke.setEnabled(false);
    }
    /**
     * Wybranie priorytetu leku
     */
    public void addListenerOnRadioButton() {

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                wybranyPriorytet = R.id.priorytetNormalny == checkedId ? 0 : 1;
            }
        });
    }
    /**
     * Wprowadzenie zalecenia żywieniowego do leku
     */
    public void addListenerOnRadioButton2() {

        radioGroup2 = (RadioGroup) findViewById(R.id.radioGroup2);

        radioGroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.zaleceniePrzed:
                        wybraneZalecenieZywnosciowe = 0;
                        break;
                    case R.id.zaleceniePodczas:
                        wybraneZalecenieZywnosciowe = 1;
                        break;
                    case R.id.zaleceniePo:
                        wybraneZalecenieZywnosciowe = 2;
                        break;
                }
            }
        });
    }

    /**
     *
     * @return aktualna data w formaciedd.mm.yyyy
     */
    private String currentDate() {
        Time t = new Time();
        t.setToNow();
        String year = String.valueOf(t.year);
        String month = (t.month+1) < 10 ? 0+ String.valueOf(t.month+1) : String.valueOf(t.month+1);
        String day = t.monthDay < 10 ? 0 + String.valueOf(t.monthDay) : String.valueOf(t.monthDay);
        return day + "." + month + "." + year;
    }

    /**
     *
     * dodaje lek do bazy
     *
     * @param nazwaLeku
     * @param rodzajID
     * @param iloscOpakowan
     * @param iloscSztuk
     * @param wybranyPriorytet
     * @param notatka
     * @param wybraneZalecenieZywnosciowe
     * @param dataWaznosci
     * @return id leku uzytkownika
     */
    private long dodajLekDoBazy(String nazwaLeku, int rodzajID, int iloscOpakowan, int iloscSztuk, int wybranyPriorytet, String notatka, int wybraneZalecenieZywnosciowe, String dataWaznosci) {

        long idLekuUzytkownika = -1;
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            int idLeku = (int) databaseHandler.addMedicine(nazwaLeku, 0, rodzajID, 0);
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            idLekuUzytkownika = databaseHandler.addUsersMedicine(sharedPreferences.getInt("id", 1), idLeku, iloscOpakowan, iloscSztuk, dataWaznosci, wybranyPriorytet, notatka, wybraneZalecenieZywnosciowe);
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return idLekuUzytkownika;
    }

    /**
     * kolorowani zaznaczonego rodzaju leku
     *
     * @param v
     */
    private void changeAlpha(View v) {
        imagebtn1.setAlpha(0.30f);
        imagebtn2.setAlpha(0.30f);
        imagebtn3.setAlpha(0.30f);
        imagebtn4.setAlpha(0.30f);
        imagebtn5.setAlpha(0.30f);
        imagebtn6.setAlpha(0.30f);
        imagebtn7.setAlpha(0.30f);
        imagebtn8.setAlpha(0.30f);

        v.setAlpha(1.0f);
        wybranyRodzaj = rodzajeMap.get(v.getId());
    }

    /**
     * podswietlanie wybranego koloru leku
     *
     * @param vie
     */
    private void changeAlpha2(View vie) {
        green_button.setAlpha(0.35f);
        blue_button.setAlpha(0.35f);
        red_button.setAlpha(0.35f);
        yellow_button.setAlpha(0.35f);
        white_button.setAlpha(0.35f);
        gray_button.setAlpha(0.35f);
        violet_button.setAlpha(0.35f);
        orange_button.setAlpha(0.35f);
        pink_button.setAlpha(0.35f);
        brown_button.setAlpha(0.35f);

        vie.setAlpha(1.0f);
    }

    /**
     * dodaje alarm wygasniecia daty waznosci leku
     *
     *
     * @param mediName
     */
    private void addExpDateAlarms(String mediName) {

        ArrayList<String> expMedsList = new ArrayList<>();
        expMedsList.add(mediName);

        Intent expIntent = new Intent(getApplicationContext(), ExpDateActivity.class);
        expIntent.putExtra("expList", expMedsList);
        PendingIntent expAlarmIntent = PendingIntent.getActivity(getApplicationContext(), 1, expIntent, PendingIntent.FLAG_ONE_SHOT);

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd" + "-" + "HH:mm", Locale.getDefault());
        Date itemDate = new Date();
//        String currentStringDate;
//        try {
//            currentStringDate = Common.currentStringDate();
//            itemDate = dateFormat.parse(currentStringDate + "-" + "00:20");
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTime(itemDate);
        calendar.add(Calendar.MINUTE, 1);
        calendar.set(Calendar.SECOND, 0);

        System.out.println(expMedsList.size() + "||" + System.currentTimeMillis() +"||"+ calendar.getTimeInMillis());

        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);


        if(Build.VERSION.SDK_INT != 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), expAlarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), expAlarmIntent);


    }

    /**
     * dodaje notkatke z okna dialogowego
     *
     * @param text
     */
    public void dodajNotatke(String text) {
        notatka = text;
    }

    /**
     * zamyka aktywnosc
     */
    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}