package mediminder.com.mediminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DatabaseHandler {

    //wersja bazy
    private static final int DATABASE_VERSION = 13;

    //nazwa bazy
    private static final String DATABASE_NAME = "mediminderDB.db";

    //nazwy tabel
    private static final String TABLE_USERS = "users";
    private static final String TABLE_MEDICINES="medicines";
    private static final String TABLE_USERS_MEDICINES="users_medicines";
    private static final String TABLE_MEDICINE_REMINDER = "medicines_reminders";
    private static final String TABLE_USER_REMINDER = "users_reminders";
    private static final String TABLE_REMINDER_MEDICINE_DAY = "reminder_medicine_days";
    private static final String TABLE_USER_SETTINGS = "user_settings";

    //klucze bazy użytkowników
    public static final String KEY_USER_ID = "_id";
    public static final String KEY_LOGIN = "_login";
    public static final String KEY_PASSWORD = "_password";
    public static final String KEY_ILLNESS = "_illness";
    public static final String KEY_ALLERGIES = "_allergies";
    public static final String KEY_BLOOD_TYPE = "_blood_type";
    public static final String KEY_PARENT_ID = "_parent_id";


    //kolumny bazy użytkowników
    public static final int USER_ID_COLUMN = 0;
    public static final int LOGIN_COLUMN = 1;
    public static final int PASSWORD_COLUMN = 2;
    public static final int ILLNESS_COLUMN = 3;
    public static final int ALLERGIES_COLUMN = 4;
    public static final int BLOOD_TYPE_COLUMN = 5;

    //klucze bazy leków
    public static final String KEY_MEDICINE_ID = "_medicine_id";
    public static final String KEY_MEDICINE_NAME = "_medicine_name";
    public static final String KEY_MEDICINE_DOSE = "_medicine_dose";
    public static final String KEY_MEDICINE_TYPE = "_medicine_type";
    public static final String KEY_MEDICINE_COLOR = "_medicine_color";

    //kolumny bazy leków
    public static final int MEDICINE_ID_COLUMN = 0;
    public static final int MEDICINE_NAME_COLUMN = 1;
    public static final int MEDICINE_DOSE_COLUMN = 2;
    public static final int MEDICINE_TYPE_COLUMN = 3;
    public static final int MEDICINE_COLOR_COLUMN = 4;

    //klucze bazy leków użytkowników (nowe)
    public static final String KEY_USER_MEDICINE_ID = "_user_medicine_id";
    public static final String KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES = "_amount_of_packages";
    public static final String KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE = "_max_quantity_in_package";
    public static final String KEY_USER_MEDICINE_ACTUAL_QUANTITY = "_actual_quantity";
    public static final String KEY_USER_MEDICINE_EXPIRATION_DATE = "_expiration_date";
    public static final String KEY_USER_MEDICINE_PRIORITY = "_priority";
    public static final String KEY_USER_MEDICINE_NOTE = "_note";
    public static final String KEY_USER_MEDICINE_FOOD = "_food_info";

    //kolumny bazy leków użytkowników
    public static final int USER_MEDICINE_ID_COLUMN = 0;
    public static final int USER_MEDICINE_USER_ID = 1;
    public static final int USER_MEDICINE_MEDICINE_ID = 2;
    public static final int USER_MEDICINE_AMOUNT_OF_PACKAGES = 3;
    public static final int USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE =4;
    public static final int USER_MEDICINE_ACTUAL_QUANTITY =5;
    public static final int USER_MEDICINE_EXPIRATION_DATE =6;
    public static final int USER_MEDICINE_PRIORITY = 7;
    public static final int USER_MEDICINE_NOTE = 8;
    public static final int USER_MEDICINE_FOOD = 9
            ;
    //klucze bazy przypomnien lekow
    public static final String KEY_MEDICINE_REMINDER_ID = "_medicine_reminder_id";
    public static final String KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT = "_dosage_amount";

    //kolumny bazy przypomnien lekow
    public static final int MEDICINE_REMINDER_ID_COLUMN = 0;
    public static final int MEDICINE_REMINDER_USER_MEDICINE_ID_COLUMN = 1;
    public static final int MEDICINE_REMINDER_USER_REMINDER_ID_COLUMN = 2;
    public static final int MEDICINE_REMINDER_DOSAGE_AMOUNT_COLUMN = 3;

    //klucze bazy przypomnien uzytkownikow
    public static final String KEY_USER_REMINDER_ID = "_user_reminder_id";
    public static final String KEY_USER_REMINDER_DATE_START = "_date_start";
    public static final String KEY_USER_REMINDER_DATE_END = "_date_end";
    //public static final String KEY_USER_REMINDER_CREATION_DATE = "_creation_date";
    public static final String KEY_USER_REMINDER_TIME = "_time";
    public static final String KEY_USER_REMINDER_NOTE = "_user_reminder_note";

    //kolumny bazy przypomnien uzytkownikow
    public static final int USER_REMINDER_ID_COLUMN = 0;
    public static final int USER_REMINDER_USER_ID_COLUMN = 1;
    public static final int USER_REMINDER_DATE_START_COLUMN = 2;
    public static final int USER_REMINDER_DATE_END_COLUMN = 3;
    public static final int USER_REMINDER_TIME_COLUMN = 4;
    public static final int USER_REMINDER_NOTE_COLUMN = 5;

    //klucze bazy statusów leków przypomnienia
    public static final String KEY_REMINDER_MEDICINE_DAY_ID = "_reminder_medicine_day_id";
    public static final String KEY_REMINDER_MEDICINE_DAY_DATE = "_date";
    public static final String KEY_REMINDER_MEDICINE_DAY_STATUS = "_medicine_status";

    //kolumny bazy statusów leków przypomnienia
    public static final int REMINDER_MEDICINE_DAY_ID_COLUMN = 0;
    public static final int REMINDER_MEDICINE_DAY_MEDICINE_REMINDER_ID_COLUMN = 1;
    public static final int REMINDER_MEDICINE_DAY_DATE_COLUMN = 2;
    public static final int REMINDER_MEDICINE_DAY_STATUS_COLUMN = 3;

    /*private static final String CREATE_USER_SETTINGS_TABLE = "CREATE TABLE " +
            TABLE_USER_SETTINGS + "("
            + "_id_settings" + " INTEGER PRIMARY KEY,"
            + KEY_USER_ID + " INTEGER,"
            + "_sound" + " TEXT,"
            + "_rem_enabled" + " INTEGER,"
            + "_number_of_repeats" + " INTEGER"
            + ");";*/

    //tworzenie tabeli użytkowników
    private static final String CREATE_USER_TABLE = "CREATE TABLE " +
            TABLE_USERS + "("
            + KEY_USER_ID + " INTEGER PRIMARY KEY,"
            + KEY_LOGIN + " TEXT,"
            + KEY_PASSWORD + " TEXT,"
            + KEY_ILLNESS + " TEXT,"
            + KEY_ALLERGIES + " TEXT,"
            + KEY_BLOOD_TYPE + " TEXT"
            + ");";

    //tworzenie tabeli leków
    private static final String CREATE_MEDICINES_TABLE = "CREATE TABLE " +
            TABLE_MEDICINES + "("
            + KEY_MEDICINE_ID + " INTEGER PRIMARY KEY,"
            + KEY_MEDICINE_NAME + " TEXT,"
            + KEY_MEDICINE_DOSE + " INTEGER,"
            + KEY_MEDICINE_TYPE + " INTEGER,"
            + KEY_MEDICINE_COLOR + " INTEGER"
            + ");";

    //tworzenie tabeli leków użytkowników
    private static final String CREATE_USERS_MEDICINES_TABLE = "CREATE TABLE " +
            TABLE_USERS_MEDICINES + "("
            + KEY_USER_MEDICINE_ID + " INTEGER PRIMARY KEY,"
            + KEY_USER_ID + " INTEGER,"
            + KEY_MEDICINE_ID + " INTEGER,"
            + KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES + " INTEGER,"
            + KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE + " INTEGER,"
            + KEY_USER_MEDICINE_ACTUAL_QUANTITY + " INTEGER,"
            + KEY_USER_MEDICINE_EXPIRATION_DATE + " TEXT,"
            + KEY_USER_MEDICINE_PRIORITY + " INTEGER,"
            + KEY_USER_MEDICINE_NOTE + " TEXT,"
            + KEY_USER_MEDICINE_FOOD + " INTEGER"
            + ");";

    private static final String CREATE_MEDICINE_REMINDER_TABLE = "CREATE TABLE " +
            TABLE_MEDICINE_REMINDER + "("
            + KEY_MEDICINE_REMINDER_ID + " INTEGER PRIMARY KEY,"
            + KEY_USER_MEDICINE_ID + " INTEGER,"
            + KEY_USER_REMINDER_ID + " INTEGER,"
            + KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT + " INTEGER"
            + ");";

    private static final String CREATE_USER_REMINDER_TABLE = "CREATE TABLE " +
            TABLE_USER_REMINDER + "("
            + KEY_USER_REMINDER_ID + " INTEGER PRIMARY KEY,"
            + KEY_USER_ID + " INTEGER,"
            + KEY_USER_REMINDER_DATE_START + " TEXT,"
            + KEY_USER_REMINDER_DATE_END + " TEXT,"
            + KEY_USER_REMINDER_TIME + " TEXT,"
            + KEY_USER_REMINDER_NOTE + " STRING"
            + ");";

    private static final String CREATE_REMINDER_MEDICINE_DAY_TABLE = "CREATE TABLE " +
            TABLE_REMINDER_MEDICINE_DAY + "("
            + KEY_REMINDER_MEDICINE_DAY_ID + " INTEGER PRIMARY KEY,"
            + KEY_MEDICINE_REMINDER_ID + " INTEGER,"
            + KEY_REMINDER_MEDICINE_DAY_DATE + " TEXT,"
            + KEY_REMINDER_MEDICINE_DAY_STATUS + " INTEGER"
            + ");";

    private SQLiteDatabase db;
    private Context context;
    private DatabaseHelper dbHelper;

    public boolean editUserMedicine(long idLekuUzytkownika, int aktualnaLiczbaOpakowan, int aktualnaIlosc, int wybranyPriorytet) {
        try {
            ContentValues values = new ContentValues();
            values.put(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES, aktualnaLiczbaOpakowan);
            values.put(KEY_USER_MEDICINE_ACTUAL_QUANTITY, aktualnaIlosc);
            values.put(KEY_USER_MEDICINE_PRIORITY, wybranyPriorytet);
            return db.update(TABLE_USERS_MEDICINES, values, "_user_medicine_id =" + idLekuUzytkownika, null) > 0;
        }
        catch (Exception e)
        {
            System.out.println("Błąd z edytowaniem leku o ID: " + e.toString());
        }

        return false;
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        public DatabaseHelper(Context context, String name, CursorFactory factory, int version){
            super(context,name,factory,version);
        }

        @Override
        public void onCreate(SQLiteDatabase db)
        {
            db.execSQL(CREATE_USER_TABLE);
            db.execSQL(CREATE_MEDICINES_TABLE);
            db.execSQL(CREATE_USERS_MEDICINES_TABLE);
            db.execSQL(CREATE_MEDICINE_REMINDER_TABLE);
            db.execSQL(CREATE_USER_REMINDER_TABLE);
            db.execSQL(CREATE_REMINDER_MEDICINE_DAY_TABLE);
            //db.execSQL(CREATE_USER_SETTINGS_TABLE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
        {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS_MEDICINES);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_MEDICINE_REMINDER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_REMINDER);
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_REMINDER_MEDICINE_DAY);
            //db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SETTINGS);
            onCreate(db);
        }
    }

    public DatabaseHandler(Context context)
    {
        this.context = context;
    }

    public DatabaseHandler open()
    {
        dbHelper = new DatabaseHelper(context, DATABASE_NAME, null, DATABASE_VERSION);
        try
        {
            db = dbHelper.getWritableDatabase();
        }
        catch (SQLException e)
        {
            db = dbHelper.getReadableDatabase();
        }
        return this;
    }

    public void close()
    {
        dbHelper.close();
    }

    /**
     * Dodawanie użytkownika do bazy
     * @param login login użytkownika
     * @param password hasło użytkownika
     * @return
     */
    public long addUser(String login, String password)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_LOGIN, login);
        values.put(KEY_PASSWORD, password);
        return db.insert(TABLE_USERS, null, values);
    }

    /**
     * Usunięcie użytkownika z bazy
     * @param userID id użytkownika
     * @return true jeśli tabela zawiera więcej niż jednego użytkownika (nie jest pusta)
     */
    public boolean deleteUser(int userID)
    {
        String where = "_id =" + userID;
        return db.delete(TABLE_USERS, where, null) > 0;
    }

    /**
     * pobranie hasła z bazy na podstawie loginu
     * @param login login użytkownika
     * @return string zawierający hasło
     */
    public String getPassword(String login)
    {
        String password = "";
        String[] columns = {"_id", "_login", "_password"};
        String where = "_login = '" + login + "'";

        try {
            Cursor cursor = db.query(TABLE_USERS, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                password = cursor.getString(PASSWORD_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem hasła:" + e.toString());
        }
        return password;
    }

    /**
     * miana hasła użytkownika
     * @param userID id użytkownika
     * @param newPassword nowe hasło
     * @return true jeśli tabela zawiera więcej niż 1 wpis
     */
    public boolean changePassword(int userID, String newPassword)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userID);
        values.put(KEY_PASSWORD, newPassword);

        return db.update(TABLE_USERS, values, "_id =" + userID, null) > 0;
    }

    public String getUserName(int userID)
    {
        String name = "";
        String where = "_id =" + userID;

        String [] columns = {"_id", "_login", "_password"};

        try {
            Cursor cursor = db.query(TABLE_USERS, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                name = cursor.getString(LOGIN_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem nazwy uzytkownika:" + e.toString());
        }
        return name;
    }

    /**
     * Funkcja do odczytu ID dla danego loginu
     * @param login login użytkownika
     * @return id użytkownika o loginie login
     */
    public int getUserID(String login)
    {
        int id = 0;
        String where = "_login = '" + login + "'";
        String[] columns = {"_id", "_login","_password"};

        try {
            Cursor cursor = db.query(TABLE_USERS, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                id = cursor.getInt(USER_ID_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem id uzytkownika:" + e.toString());
        }
        return id;
    }

    /**
     * Sprawdzenie czy istnieje użytkownik o podanym id
     * @param login login użytkownika
     * @return 0 jeśli istnieje
     */
    public int checkIfExists(String login)
    {
        int tmp = 0;
        //String where = "_login = '" + login + "'";
        //String[] columns = {"_id","_login","_password"};
        try{
            Cursor cursor = db.rawQuery("SELECT EXISTS(SELECT 1 FROM users WHERE _login = '"+login+"' LIMIT 1)",null);
            if (cursor != null && cursor.moveToFirst()) {
                tmp = cursor.getInt(0);
            }
        }
        catch(Exception e) {
            System.out.println("Blad ze sprawdzeniem czy uzytkownik istnieje");
        }
        return tmp;
    }

    /**
     * Zapis danych z profilu użytkownika.
     * @param id id użytkownika
     * @param illness choroba użytkownika
     * @param allergies alergie użytkownika
     * @param bloodType grupa krwi użytkownika
     * @return true jeśli tabela nie jest pusta
     */
    public boolean fillUserProfile(int id, String illness, String allergies, String bloodType)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_ILLNESS, illness);
        values.put(KEY_ALLERGIES, allergies);
        values.put(KEY_BLOOD_TYPE, bloodType);

        return db.update(TABLE_USERS, values, "_id =" + id, null) > 0;
    }

    /**
     * Wpis do bazy choroby użytkownika
     * @param id id użytkownika
     * @param illness choroba użytkownika
     * @return true jeśli tabela nie jest pusta
     */
    public boolean fillUserIllness(int id, String illness)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_ILLNESS, illness);

        return db.update(TABLE_USERS, values, "_id =" + id, null) > 0;
    }

    /**
     * Wpis do bazy alergii użytkownika
     * @param id id użytkownika
     * @param allergies alergie użytkownika
     * @return true jeśli tabela nie jest pusta
     */
    public boolean fillUserAllergies(int id, String allergies)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_ALLERGIES, allergies);

        return db.update(TABLE_USERS, values, "_id =" + id, null) > 0;
    }

    /**
     * Wpis do bazy grupy krwi użytkownika.
     * @param id id użytkownika
     * @param bloodType grupa krwi użytkownika
     * @return true jeśli tabela nie jest pusta
     */
    public boolean fillUserBloodType(int id, String bloodType)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_BLOOD_TYPE, bloodType);

        return db.update(TABLE_USERS, values, "_id =" + id, null) > 0;
    }

    public String[] getUserProfileData(int id)
    {
       String[] userProfileData = new String[6];
       String where = "_id =" + id;

        String[] columns = {"_id", "_login", "_password", "_illness", "_allergies", "_blood_type"};

        try {
            Cursor cursor = db.query(TABLE_USERS, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                userProfileData[0] = Integer.toString(cursor.getInt(USER_ID_COLUMN));
                userProfileData[1] = cursor.getString(LOGIN_COLUMN);
                userProfileData[2] = cursor.getString(PASSWORD_COLUMN);
                userProfileData[3] = cursor.getString(ILLNESS_COLUMN);
                userProfileData[4] = cursor.getString(ALLERGIES_COLUMN);
                userProfileData[5] = cursor.getString(BLOOD_TYPE_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem danych użytkownika: " + e.toString());
        }

       return userProfileData;
    }

   /* public long addSettings(int userID, String sound, int rem_enabled, int numberOfRepeats)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userID);
        values.put("_sound", sound);
        values.put("_rem_enabled", rem_enabled);
        values.put("_number_of_repeats", numberOfRepeats);
        return db.insert(TABLE_USER_SETTINGS, null, values);
    }*/

    /*public String[] getSettings(int userID)
    {
        String[] settings = new String[3];
        String where = "_id =" + userID;

        String[] columns = {"_id_settings", "_id", "_sound", "_rem_enabled", "_number_of_repeats"};

        try {
            Cursor cursor = db.query(TABLE_USER_SETTINGS, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                settings[0] = cursor.getString(2); //sound
                settings[1] = cursor.getString(3); //rem_enabled
                settings[2] = cursor.getString(4); //numOfRepeats
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem ustawień użytkownika: " + e.toString());
        }
        return settings;
    }*/

    /**
     * dodawanie leku do bazy leków
     * @param medicine nazwa leku
     * @param dose dawka leku
     * @param type typ leku
     * @param color kolor leku
     * @return
     */
    public long addMedicine(String medicine, int dose, int type, int color)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_MEDICINE_NAME, medicine);
        values.put(KEY_MEDICINE_DOSE, dose);
        values.put(KEY_MEDICINE_TYPE, type);
        values.put(KEY_MEDICINE_COLOR, color);
        return db.insert(TABLE_MEDICINES, null, values);
    }

    public String[] getMedicine(int medicineID)
    {
        String[] name = new String[3];
        String where = "_medicine_id =" + medicineID;

        String[] columns = {"_medicine_id", "_medicine_name", "_medicine_dose", "_medicine_type", "_medicine_color"};

        try {
            Cursor cursor = db.query(TABLE_MEDICINES, columns, where, null, null, null, null);
            if (cursor != null && cursor.moveToFirst()) {
                name[0] = cursor.getString(MEDICINE_NAME_COLUMN);
                name[1] = cursor.getString(MEDICINE_DOSE_COLUMN);
                name[2] = cursor.getString(MEDICINE_TYPE_COLUMN);
                name[3] = cursor.getString(MEDICINE_COLOR_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem leku: " + e.toString());
        }
        return name;
    }

    //sprawdzenie ilości leku w bazie
    public String checkMedicinesQuantity(int medicineID)
    {
        String quantity = "";
        String where = "_medicine_id =" + medicineID;

        String[] columns = {"_medicine_id", "_medicine_name", "_medicine_dose", "_medicine_type", "_medicine_color"};

        try {
            Cursor cursor = db.query(TABLE_MEDICINES, columns, where, null, null, null,null);
            if (cursor != null && cursor.moveToFirst()) {
                quantity = cursor.getString(MEDICINE_DOSE_COLUMN);
            }
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem ilości leku:" + e.toString());
        }
        return quantity;

    }

    /**
     * Usunięcie leku z bazy.
     * @param medicineID id leku
     * @return zakutalizowana tabela
     */
    public boolean deleteMedicine(int medicineID)
    {
        String where = "_medicine_id =" + medicineID;
        return db.delete(TABLE_MEDICINES, where, null) > 0;
    }


    //pobranie listy wszystkich leków
    public List<String> getAllMedicines()
    {
        List<String> list = new ArrayList<String>();

        Cursor cursor = db.rawQuery("SELECT _medicine_name FROM medicines", null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String name = cursor.getString(cursor.getColumnIndex(KEY_MEDICINE_NAME));
                list.add(name);
                cursor.moveToNext();
            }
        }
        cursor.close();
        return list;
    }

    /**
     * Pobranie wszystkich leków użytkownika.
     * @param userID id użytkownika
     * @return Lista leków typu MediItem
     */
    public ArrayList<MediItem> getAllUsersMedicines(int userID)
    {
        ArrayList<MediItem> userMedicineList = new ArrayList<MediItem>();

        Cursor cursor = db.rawQuery("SELECT _user_medicine_id, _medicine_id, _amount_of_packages, _max_quantity_in_package, _actual_quantity, _expiration_date, _priority, _note, _food_info FROM users_medicines WHERE _id =" + userID, null);

        if(cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                int userMedicineID = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_ID));
                int medicineID = cursor.getInt(cursor.getColumnIndex(KEY_MEDICINE_ID));
//                int amountOfPackages = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES));
//                int maxQuantityInPackage = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE));
                int actualQuantity = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_ACTUAL_QUANTITY));
                String expirationDate = cursor.getString(cursor.getColumnIndex(KEY_USER_MEDICINE_EXPIRATION_DATE));
                int priority = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_PRIORITY));
                String note = cursor.getString(cursor.getColumnIndex(KEY_USER_MEDICINE_NOTE));
                int wybraneZalecenieZywnosciowe = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_FOOD));

                Cursor cursor2 = db.rawQuery("SELECT _medicine_name, _medicine_dose, _medicine_type, _medicine_color FROM medicines WHERE _medicine_id =" + medicineID, null);

                if (cursor2.moveToFirst()) {
                    while (!cursor2.isAfterLast()) {
                        String medicineName = cursor2.getString(cursor2.getColumnIndex(KEY_MEDICINE_NAME));
                        int medicineDose = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_DOSE));
                        int medicineType = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_TYPE));
                        int medicineColor = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_COLOR));

                        userMedicineList.add(new MediItem(userMedicineID, medicineName, medicineDose, medicineType, medicineColor, actualQuantity,expirationDate));
//                        userMedicineList.add(new MediItem(userMedicineID, medicineID, medicineName, medicineDose, medicineType, medicineColor, amountOfPackages, maxQuantityInPackage, actualQuantity, expirationDate, priority, note, foodInfo)); TODO:dodane foodInfo, czyli informacja o nawykach żywieniowych

                        cursor2.moveToNext();
                    }
                }
                cursor2.close();
                cursor.moveToNext();
            }
        }
        cursor.close();

        return userMedicineList;
    }

    //dodanie samej notatki
    public boolean addUserMedicineNote(String id, String note)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_MEDICINE_NOTE, note);
        return db.update(TABLE_USERS_MEDICINES, values, "_user_medicine_id =" + id, null) > 0;
    }

    /**
     * Pobranie wybranych leków użytkownika.
     * @param userMedicinesIds numery ID leków
     * @param areIn
     * @return lista leków typu MediItem
     */
    public ArrayList<MediItem> getSelectedUserMedicines(ArrayList<Integer> userMedicinesIds, boolean areIn)
    {
        ArrayList<MediItem> userMedicineList = new ArrayList<MediItem>();

        Cursor cursor = db.rawQuery("SELECT _user_medicine_id, _medicine_id, _amount_of_packages, _max_quantity_in_package, _actual_quantity, _expiration_date, _priority, _note, _food_info FROM users_medicines WHERE _user_medicine_id " + (!areIn ? "NOT" : "") + " IN(" +makePlaceholders(userMedicinesIds) + ")", null);

        if(cursor.moveToFirst())
        {
            while(!cursor.isAfterLast())
            {
                int userMedicineID = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_ID));
                int medicineID = cursor.getInt(cursor.getColumnIndex(KEY_MEDICINE_ID));
                int amountOfPackages = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES));
                int maxQuantityInPackage = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE));
                int actualQuantity = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_ACTUAL_QUANTITY));
                String expirationDate = cursor.getString(cursor.getColumnIndex(KEY_USER_MEDICINE_EXPIRATION_DATE));
                int priority = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_PRIORITY));
                String note = cursor.getString(cursor.getColumnIndex(KEY_USER_MEDICINE_NOTE)); //TODO: sprawdzić czy inne klasy to potrzebują z tej funkcji
                int foodInfo = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_FOOD));

                Cursor cursor2 = db.rawQuery("SELECT _medicine_name, _medicine_dose, _medicine_type, _medicine_color FROM medicines WHERE _medicine_id =" + medicineID, null);

                if (cursor2.moveToFirst()) {
                    while (!cursor2.isAfterLast()) {
                        String medicineName = cursor2.getString(cursor2.getColumnIndex(KEY_MEDICINE_NAME));
//                        int medicineDose = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_DOSE));
                        int medicineType = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_TYPE));
                        int medicineColor = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_COLOR));

                        userMedicineList.add(new MediItem(userMedicineID, medicineName, medicineType,
                                medicineColor, actualQuantity, amountOfPackages, maxQuantityInPackage, expirationDate,
                                priority, note, foodInfo));
//                        userMedicineList.add(new MediItem(userMedicineID, medicineID, medicineName, medicineDose, medicineType, medicineColor, amountOfPackages, maxQuantityInPackage, actualQuantity, expirationDate, priority, note, foodInfo));

                        cursor2.moveToNext();
                    }
                }
                cursor2.close();
                cursor.moveToNext();
            }
        }
        cursor.close();

        return userMedicineList;
    }

    String makePlaceholders(ArrayList<Integer> ids) {
        int len = ids.size();
        if (len < 1) {
            // It will lead to an invalid query anyway ..
            return "";
        } else {
            StringBuilder sb = new StringBuilder(len);
            sb.append(ids.get(0));
            for (int i = 1; i < len; i++) {
                sb.append(","+ids.get(i));
            }
            return sb.toString();
        }
    }

    /**
     * Dodanie leku użytkownika (do bazy leków użytkowników)
     * @param userID id użytkownika
     * @param medicineID id leku
     * @param amount_of_packages liczba opakowań
     * @param quantity_in_package ilość leku w opakowaniu
     * @param expiration_date data ważności leku
     * @param priority priorytet leku
     * @param note notatka do leku
     * @param wybraneZalecenieZywnosciowe zalecenia żywieniowe do leku
     * @return
     */
    public long addUsersMedicine(int userID, int medicineID, int amount_of_packages, int quantity_in_package, String expiration_date, int priority, String note, int wybraneZalecenieZywnosciowe)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userID);
        values.put(KEY_MEDICINE_ID, medicineID);
        values.put(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES, amount_of_packages);
        values.put(KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE, quantity_in_package);
        values.put(KEY_USER_MEDICINE_ACTUAL_QUANTITY, amount_of_packages * quantity_in_package);
        values.put(KEY_USER_MEDICINE_EXPIRATION_DATE, expiration_date);
        values.put(KEY_USER_MEDICINE_PRIORITY, priority);
        values.put(KEY_USER_MEDICINE_NOTE, note);
        values.put(KEY_USER_MEDICINE_FOOD, wybraneZalecenieZywnosciowe); //-1, 0 lub 1 - oznaczają odpowiednio przed jedzieniem, w trakcie, po jedzeniu. Inna wartość oznacza, że nie ma to znaczenia

        return db.insert(TABLE_USERS_MEDICINES, null, values);
    }

    /**
     * Edycja ilości leku użytkownika
     * @param userMedicineID ID leku użytkownika
     * @param dosage dawka leku
     * @return
     */
    public boolean editUserMedicineQuantity(int userMedicineID, int dosage)
    {

        try {
            Cursor cursor = db.rawQuery("SELECT _amount_of_packages, _max_quantity_in_package, _actual_quantity FROM users_medicines WHERE _user_medicine_id =" + userMedicineID, null);
            if (cursor != null && cursor.moveToFirst()) {
                int amountOfPackages = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES));
                int maxQuantityInPackage = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_MAX_QUANTITY_IN_PACKAGE));
                int actualQuantity = cursor.getInt(cursor.getColumnIndex(KEY_USER_MEDICINE_ACTUAL_QUANTITY));

                int after = actualQuantity - dosage;

                ContentValues values = new ContentValues();

                if(after % maxQuantityInPackage == 0) {
                    amountOfPackages--;
                    values.put(KEY_USER_MEDICINE_AMOUNT_OF_PACKAGES, amountOfPackages);
                }

                values.put(KEY_USER_MEDICINE_ACTUAL_QUANTITY, after < 0 ? 0 : after);

                return db.update(TABLE_USERS_MEDICINES, values, "_user_medicine_id =" + userMedicineID, null) > 0;
            }
            cursor.close();
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem leku o ID: " + e.toString());
        }

        return false;
    }

    /**
     * Pobranie ID leku użytkownika z bazy leków
     * @param userMedicineID ID leku użytkownika
     * @return id szukanego leku
     */
    public int getMedicineID(int userMedicineID)
    {
        int medicineID = 0;

        try {
            Cursor cursor = db.rawQuery("SELECT _medicine_id FROM users_medicines WHERE _user_medicine_id =" + userMedicineID, null);
            if (cursor != null && cursor.moveToFirst()) {
                medicineID = cursor.getInt(cursor.getColumnIndex(KEY_MEDICINE_ID));
            }
            cursor.close();
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem ID leku: " + e.toString());
        }
        return medicineID;
    }

    /**
     * Usunięcie leków użytkownika z przypomnienia
     * @param userMedicineID ID leku użytkownika
     * @return
     */
    public boolean deleteUserReminderMedicines(int userMedicineID) //używane przy usuwanie leku. usuwa lek z przypomnien
    {
        try {
            Cursor cursor = db.rawQuery("SELECT _medicine_reminder_id, _user_reminder_id FROM medicines_reminders WHERE _user_medicine_id =" + userMedicineID, null);
            if (cursor != null && cursor.moveToFirst()) {
                while(!cursor.isAfterLast()) {

                    int medicine_reminder_id = cursor.getInt(cursor.getColumnIndex(KEY_MEDICINE_REMINDER_ID));
                    deleteReminderMedicineDays(medicine_reminder_id);

                    int userReminderID = cursor.getInt(cursor.getColumnIndex(KEY_USER_REMINDER_ID));
                    ArrayList<PillItem> pills = getUserReminderMedicines(userReminderID, "");
                    if (pills.size() == 1) { // jesli zostal jedene lek w przypomnieniu uzytkownika to usuwamy przypomnienie bo ten lek jest do usuniecia
                        String where = "_user_reminder_id =" + userReminderID;
                        db.delete(TABLE_USER_REMINDER, where, null);
                    }
                    cursor.moveToNext();
                }
            }
            cursor.close();
        }
        catch (Exception e)
        {
            System.out.println("Błąd " +
                    "pobierania przypomnienia na podstawie leku użytkownika: " + e.toString());
        }

        String where = "_user_medicine_id =" + userMedicineID;
        return db.delete(TABLE_MEDICINE_REMINDER, where, null) > 0;
    }

    /**
     * Usuwa wiersze z tabeli ze statusami lekow przypomnien dotyczace podanego leku przypomnienia
     * @param medicine_reminder_id ID przypomnienia leku
     * @return
     */
    private boolean deleteReminderMedicineDays(int medicine_reminder_id) { // usuwa wiersze z tabeli ze statusami lekow przypomnien dotyczace podanego leku przypomnienia
        try {
            String where = "_medicine_reminder_id =" + medicine_reminder_id;
            return db.delete(TABLE_REMINDER_MEDICINE_DAY, where, null) > 0;
        }
        catch (Exception e)
        {
            System.out.println("Błąd " +
                    "pobierania przypomnienia na podstawie leku użytkownika: " + e.toString());
        }

        return false;
    }

    public boolean deleteUsersMedicine(int userMedicineID)
    {
        deleteUserReminderMedicines(userMedicineID); //usuwa najpierw lek uzytkownika z lekow jego przypomnien

        int medicineID = this.getMedicineID(userMedicineID);
        this.deleteMedicine(medicineID);

        String where = "_user_medicine_id =" + userMedicineID;
        return db.delete(TABLE_USERS_MEDICINES, where, null) > 0;
    }


    //pobranie id ostatniego rekordu z bazy przypomnienia uzytkownikow
    public int getLastUserReminderID()
    {
        int lastID = 0;

        try {
            Cursor cursor = db.rawQuery("SELECT TOP 1 * FROM users_reminders ORDER BY _user_reminder_id DESC", null);
            if (cursor != null && cursor.moveToFirst()) {
                lastID = cursor.getInt(USER_MEDICINE_MEDICINE_ID);
            }
            cursor.close();
        }
        catch (Exception e)
        {
            System.out.println("Błąd z pobraniem ostatniego ID: " + e.toString());
        }

        return lastID;
    }

    /**
     * Dodanie przypomnienia do bazy przypomnień użytkowników
     * @param date_start data początkowa przypomnienia
     * @param date_end data końcowa przypomnienia
     * @param time godzina przypomnienia
     * @param userID ID użytkownika
     * @param note notatka do przypomnienia
     * @return
     */
    public long addUserReminder(String date_start, String date_end,  String time, int userID, String note)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_ID, userID);
        values.put(KEY_USER_REMINDER_DATE_START, date_start);
        values.put(KEY_USER_REMINDER_DATE_END, date_end);
        values.put(KEY_USER_REMINDER_TIME, time);
        values.put(KEY_USER_REMINDER_NOTE, note);

        return db.insert(TABLE_USER_REMINDER, null, values);
    }

    //usuwanie po creation_date
    /*public boolean deleteUserReminder(String creation_date, int id)
    {
        String where = "_creation_date = '" + creation_date + " AND _id =" + id;
        return db.delete(TABLE_USER_REMINDER, where, null) > 0;
    }*/

    /**
     * Doddanie do bazy leków przypomnień
     * @param userMedicineID ID leku użytkownika
     * @param userReminderID ID przypomnienia użytkownika
     * @param dosage dawka leku
     * @return
     */
    public long addReminderMedicine(int userMedicineID, int userReminderID, int dosage)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_MEDICINE_ID, userMedicineID);
        values.put(KEY_USER_REMINDER_ID, userReminderID);
        values.put(KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT, dosage);
        return db.insert(TABLE_MEDICINE_REMINDER, null, values);
    }

    /**
     * Dodanie przypomnienia.
     * @param date_start data początkowa przypomnienia
     * @param date_end data końcowa przypomnienia
     * @param time godzina przypomnienia
     * @param userMedicines leku użytkownika dodawane do przypomnienia
     * @param userID ID użytkownika
     * @param note notatka do przypomnienia
     * @return
     */
    public long addReminder(String date_start, String date_end, String time, List<MedChoiceItem> userMedicines, int userID, String note)
    {
        long reminderId = this.addUserReminder(date_start, date_end, time, userID, note);

        for(int i = 0; i < userMedicines.size(); i++)
        {
            MedChoiceItem item = userMedicines.get(i);
            int medicine_reminder_id = (int) this.addReminderMedicine(item.getUserMedicineId(), (int) reminderId, item.getDosage());
            item.setUserMedicineReminderId(medicine_reminder_id);

            boolean addedReminderMedicineDays = addReminderMedicineDays(medicine_reminder_id, date_start, date_end);
        }

        return reminderId;
    }

    private boolean addReminderMedicineDays(int medicine_reminder_id, String date_start, String date_end) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        String date_current = date_start;
        try {
            Date date = dateFormat.parse(date_end);
            date = Common.addDays(date, 1);
            String dateAfterEnd = dateFormat.format(date);
            System.out.println("DATA!!!! " + dateAfterEnd);
            while(!date_current.equals(dateAfterEnd)){

                ContentValues values = new ContentValues();
                values.put(KEY_MEDICINE_REMINDER_ID, medicine_reminder_id);
                values.put(KEY_REMINDER_MEDICINE_DAY_DATE, date_current);
                values.put(KEY_REMINDER_MEDICINE_DAY_STATUS, 0);
                db.insert(TABLE_REMINDER_MEDICINE_DAY, null, values);

                Date currentDate = dateFormat.parse(date_current);
                currentDate = Common.addDays(currentDate, 1);
                date_current = dateFormat.format(currentDate);
            }
            return true;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Usunięcie leków z bazy leków przypomnień
     * @param reminderID ID przypomnienia
     * @return
     */
    public boolean deleteMedicinesFromMedicinesReminder(int reminderID)
    {
        String where = "_user_reminder_id =" + reminderID;
        return db.delete(TABLE_MEDICINE_REMINDER, where, null) > 0;
    }

    /**
     * Usuniecie przypomnienia z bazy przypomnień użytkowników
     * @param reminderID ID przypomnienia
     * @return
     */
    public boolean deleteReminderFromUser(int reminderID)
    {
        String where = "_user_reminder_id =" + reminderID;
        return db.delete(TABLE_USER_REMINDER, where, null) > 0;
    }

    private boolean deleteAllReminderMedicineDaysFromReminder(int reminderID) {

        Cursor cursor = db.rawQuery("SELECT _medicine_reminder_id FROM medicines_reminders WHERE _user_reminder_id =" + reminderID, null);

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                int medicineReminderID = cursor.getInt(cursor.getColumnIndex(KEY_MEDICINE_REMINDER_ID));
                deleteReminderMedicineDays(medicineReminderID);
                cursor.moveToNext();
            }
            return true;
        }
        else
            System.out.println("brak leków w przypomnieniu!");

        return false;
    }

    public boolean deleteReminder(int reminderID)
    {
        return this.deleteAllReminderMedicineDaysFromReminder(reminderID) && this.deleteMedicinesFromMedicinesReminder(reminderID) && this.deleteReminderFromUser(reminderID);
    }

//    public boolean editMedicineReminder(int medicineReminderID, int userMedicineID, int userReminderID, int status, String dosage, int userID)
//    {
//        ContentValues values = new ContentValues();
//        values.put(KEY_USER_MEDICINE_ID, userMedicineID);
//        values.put(KEY_USER_REMINDER_ID, userReminderID);
//        values.put(KEY_MEDICINE_REMINDER_STATUS, status);
//        values.put(KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT, dosage);
//
//        return db.update(TABLE_MEDICINE_REMINDER, values, "_medicine_reminder_id =" + medicineReminderID, null) > 0;
//    }

    /**
     * Edycja przypomnienia leku
     * @param medicineReminderID ID przypomnienia leku
     * @param status status przypomnienia
     * @param dosage dawka leku
     * @param userMedicineId ID leku użytkownika
     * @param confirmedDayDate
     * @return
     */
    public boolean editMedicineReminder(int medicineReminderID, int status, int dosage, int userMedicineId, String confirmedDayDate)
    {
        if(confirmedDayDate != null || !confirmedDayDate.equals(""))
            editReminderMedicineDay(medicineReminderID, confirmedDayDate, status);

        ContentValues values = new ContentValues();
        values.put(KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT, dosage);

        if(status == 1)
            editUserMedicineQuantity(userMedicineId, dosage);


        return db.update(TABLE_MEDICINE_REMINDER, values, "_medicine_reminder_id =" + medicineReminderID, null) > 0;
    }

    private boolean editReminderMedicineDay(int medicineReminderID, String confirmedDayDate, int status) {
        ContentValues values = new ContentValues();
        values.put(KEY_REMINDER_MEDICINE_DAY_STATUS, status);

        return db.update(TABLE_REMINDER_MEDICINE_DAY, values, "_medicine_reminder_id =" + medicineReminderID + " AND _date ='" + confirmedDayDate + "'", null) > 0;
    }

    /**
     * Edycja przypomnienia użytkownika
     * @param userReminderID ID przypomnienia użytkownika
     * @param date_start data początkowa przypomnienia
     * @param date_end data końcowa przypomnienia
     * @param time godzina przypomnienia
     * @param note notatka do przypomnienia
     * @return
     */
    public boolean editUserReminder(int userReminderID, String date_start, String date_end, String time, String note)
    {
        ContentValues values = new ContentValues();
        values.put(KEY_USER_REMINDER_DATE_START, date_start);
        values.put(KEY_USER_REMINDER_DATE_END, date_end);
        values.put(KEY_USER_REMINDER_TIME, time);
        values.put(KEY_USER_REMINDER_NOTE, note);

        return db.update(TABLE_USER_REMINDER, values, "_user_reminder_id =" + userReminderID, null) > 0;
    }

    /**
     * Edycja przypomnienia
     * @param userReminderID ID przypomnienia użytkownika
     * @param reminderMedicines lista leków w przypomnieniu
     * @param date_start data początkowa przypomnienia
     * @param date_end data końcowa przypomnienia
     * @param time godzina przypomnienia
     * @param note notatka do przypomnienia
     * @param confirmedDayDate
     * @return
     */
    public boolean editReminder(int userReminderID, List<PillItem> reminderMedicines, String date_start, String date_end,String time, String note, String confirmedDayDate)
    {
        boolean edited = this.editUserReminder(userReminderID,date_start, date_end, time, note);

        for(int i = 0; i < reminderMedicines.size(); i++)
        {
            PillItem item = reminderMedicines.get(i);
            edited = edited && this.editMedicineReminder(item.getMedicineReminderID(), item.isConfirmed() ? 1 : 0, item.getDosage(), item.getUserMedicineId(), confirmedDayDate);
        }
        return edited;
    }

    private Date getDate(String str){
        SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy");

        try {
            return  df.parse(str);
        } catch (ParseException e) {
            return null;
        }
    }

    private int getReminderMedicineDayStatus(int medicineReminderID, String date) {
        Cursor cursor = db.rawQuery("SELECT _medicine_status FROM reminder_medicine_days WHERE _medicine_reminder_id =" + medicineReminderID + " AND _date ='" + date + "'", null);
        if(cursor.moveToFirst())
            return cursor.getInt(cursor.getColumnIndex(KEY_REMINDER_MEDICINE_DAY_STATUS));
        cursor.close();
        return 0;
    }

    /**
     * Pobranie przypomnień użytkownika
     * @param userID ID użytkownika
     * @param startDate data początkowa
     * @param endDate data końcowa
     * @param pageDate
     * @return
     */
    public ArrayList<ReminderItem> getUserReminders(int userID, String startDate, String endDate, String pageDate)
    {
        ArrayList<ReminderItem> reminderList = new ArrayList<ReminderItem>();

        Cursor cursor = db.rawQuery("SELECT _user_reminder_id, _date_start, _date_end, _time, " +
                "_user_reminder_note FROM users_reminders WHERE _id =" + userID
                + " AND (( _date_start <= '" + startDate +"' AND _date_end >= '" + startDate
                + "' ) OR ( _date_start >= '" + startDate + "' AND _date_end <= '" + endDate
                + "' ) OR ( _date_start <= '" + endDate +"' AND _date_end >= '" + endDate
                + "' )) ORDER BY 4 ASC", null);

        if(cursor.moveToFirst())
        {

            while(!cursor.isAfterLast())
            {
                int userReminderID = cursor.getInt(cursor.getColumnIndex(KEY_USER_REMINDER_ID));
                String date_start = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_DATE_START));
                String date_end = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_DATE_END));
                String time = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_TIME));
                String note = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_NOTE));

                ArrayList<PillItem> pills = getUserReminderMedicines(userReminderID, pageDate);

                reminderList.add(new ReminderItem(userReminderID, date_start, date_end, time, pills, note));
                cursor.moveToNext();
            }
        } else{
            System.out.println("brak rekorów!");
        }
        cursor.close();
        return reminderList;
    }

    /**
     * Pobranie leków przypomnienia użytkownika
     * @param userReminderID ID przypomnienia użytkownika
     * @param pageDate
     * @return
     */
    private ArrayList<PillItem> getUserReminderMedicines(int userReminderID, String pageDate) {
        ArrayList<PillItem> pills = new ArrayList<PillItem>();
        Cursor cursor2 = db.rawQuery("SELECT _medicine_reminder_id, _user_medicine_id, _dosage_amount FROM medicines_reminders WHERE _user_reminder_id =" + userReminderID, null);

        if (cursor2.moveToFirst()) {
            while (!cursor2.isAfterLast()) {
                int medicineReminderID = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_REMINDER_ID));
                int userMedicineId = cursor2.getInt(cursor2.getColumnIndex(KEY_USER_MEDICINE_ID));
                int dosage = cursor2.getInt(cursor2.getColumnIndex(KEY_MEDICINE_REMINDER_DOSAGE_AMOUNT));
                int medicineStatus = getReminderMedicineDayStatus(medicineReminderID, pageDate);

                Cursor cursor3 = db.rawQuery("SELECT _medicine_id FROM users_medicines WHERE _user_medicine_id =" + userMedicineId, null);

                if(cursor3.moveToFirst()){
                    int medicineId = cursor3.getInt(cursor3.getColumnIndex(KEY_MEDICINE_ID));

                    Cursor cursor4 = db.rawQuery("SELECT _medicine_name, _medicine_type, _medicine_color FROM medicines WHERE _medicine_id =" + medicineId, null);
                    if (cursor4.moveToFirst()) {
                        String medicine_name = cursor4.getString(cursor4.getColumnIndex(KEY_MEDICINE_NAME));
                        int medicine_type = cursor4.getInt(cursor4.getColumnIndex(KEY_MEDICINE_TYPE));
                        int medicine_color = cursor4.getColumnIndex(KEY_MEDICINE_COLOR);

                        pills.add(new PillItem(medicineReminderID, medicine_name, medicine_type, medicine_color, dosage, (medicineStatus != 0), userMedicineId));
                    }
                    else
                        System.out.println("Lek o id = " + medicineId + " nie istnieje w bazie!");
                    cursor4.close();
                }
                else
                    System.out.println("Lek użytkownika o id = " + userMedicineId + " nie istnieje");
                cursor3.close();
                cursor2.moveToNext();
            }
        }
        else
            System.out.println("brak leków w przypomnieniu!");
        cursor2.close();

        return pills;
    }

    public ReminderItem getPrzypomnienie (int userID, int userReminderId, String data)
    {

        Cursor cursor = db.rawQuery("SELECT _user_reminder_id, _date_start, _date_end, _time, _user_reminder_note FROM users_reminders WHERE _id =" + userID + " AND _user_reminder_id =" + userReminderId, null);

        if(cursor.moveToFirst())
        {

                int userReminderID = cursor.getInt(cursor.getColumnIndex(KEY_USER_REMINDER_ID));
                String date_start = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_DATE_START));
                String date_end = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_DATE_END));
                String time = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_TIME));
                String note = cursor.getString(cursor.getColumnIndex(KEY_USER_REMINDER_NOTE));

                ArrayList<PillItem> pills = getUserReminderMedicines(userReminderID, data);

                return new ReminderItem(userReminderID, date_start, date_end, time, pills, note);

        } else{
            System.out.println("brak rekorów!");
        }
        cursor.close();
        return null;
    }



}
