package mediminder.com.mediminder;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Mikołaj on 2015-01-18.
 */
public class Common {

    public static String currentStringDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public static boolean checkIfReminderIsAllConfirmed(ReminderItem currentReminder) {
        for(PillItem item : currentReminder.getPills())
            if(!item.isConfirmed())
                return false;

        return true;
    }

    public static boolean checkIfReminderIsOnSelectedPage(String startDate, String stopDate, ReminderListPage page) {
        return (startDate.compareTo(page.getStartDate()) <= 0 && stopDate.compareTo(page.getStartDate()) >= 0)
                || (startDate.compareTo(page.getStartDate()) >= 0
                && stopDate.compareTo(page.getEndDate()) <= 0)
                || (startDate.compareTo(page.getEndDate()) <=0 && stopDate.compareTo(page.getEndDate()) >= 0);
    }

    public static Date addDays(Date date, int days)
    {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }

    public static BroadcastReceiver finishActivityWhenLogout(final Activity activity) {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.mediminder.com.mediminder.ACTION_LOGOUT");
        BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                activity.finish();
            }
        };
        activity.registerReceiver(broadcastReceiver, intentFilter);

        return broadcastReceiver;
    }
}
