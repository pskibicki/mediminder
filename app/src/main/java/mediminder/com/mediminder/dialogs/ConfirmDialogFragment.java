package mediminder.com.mediminder.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

import mediminder.com.mediminder.ReminderPilllsAdapter;

/**
 * Created by Mikołaj on 2014-11-01.
 */
public class ConfirmDialogFragment extends DialogFragment{

    private ReminderPilllsAdapter parentAdapter;

    public  ConfirmDialogFragment(){
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("POTWIERDZENIE")
                .setMessage(setPropertyMessage())
                .setPositiveButton("POTWIERDŹ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        parentAdapter.confirmOnDialogAction();
                    }
                })
                .setNegativeButton("ANULUJ", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    private CharSequence setPropertyMessage() {
        return parentAdapter.checkAllCheckBoxes() ? "Czy potwierdzasz zażycie wszystkich leków?" : "Czy potwierdzasz zażycie zaznaczonych leków?\nPamiętaj o zażyciu pozostałych!";
    }

    public void setParentAdapter(ReminderPilllsAdapter rPA) {
        this.parentAdapter = rPA;
    }

}
