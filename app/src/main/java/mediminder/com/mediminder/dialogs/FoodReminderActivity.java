package mediminder.com.mediminder.dialogs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mediminder.com.mediminder.Common;
import mediminder.com.mediminder.Home;
import mediminder.com.mediminder.R;

/**
 * Przypomnienie o spożyciu posiłku przed/w trakcie/po zażyciu leku.
 * Created by Basia on 2014-12-20.
 */
public class FoodReminderActivity extends Activity {


    private String data;
    private Button positiveButton;
    private Button negativeButton;
    private static final int DELAY = 1;
    private int foodId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.food_reminder);

        Intent intent = getIntent();

        data = Common.currentStringDate();

        foodId = intent.getIntExtra("foodId", 1);

        String str="";

        switch(foodId) {
            case 0: {
                str = new String("Pamiętaj aby nic nie jeść, bo za pół godziny zażywasz lek!\n");
                break;
            }
            case 1:{
                str = new String("Pamiętaj o posiłku w trakcie brania leku za pół godziny!\n");
                break;
            }
            case 2: {
                str = new String("Pamiętaj o posiłku, bo za pół godziny zażywasz lek!\n");
                break;
            }
        }
        TextView text = (TextView) findViewById(R.id.text);
        text.setText(str);


        positiveButton = (Button) findViewById(R.id.positive_button);
        negativeButton = (Button) findViewById(R.id.negative_button);

        positiveButton.setText("Ok");
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        negativeButton.setText("Przypomnij później");
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delayDialog();
            }
        });
    }


    @Override
    public void onBackPressed(){
    }

    private void delayDialog() {
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(FoodReminderActivity.this.getApplicationContext(), FoodReminderActivity.class);
        intent.putExtra("foodId", foodId);
        PendingIntent alarmIntent = PendingIntent.getActivity(FoodReminderActivity.this.getApplicationContext(), foodId, intent, 0);
        if(Build.VERSION.SDK_INT != 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        finish();
    }



}


