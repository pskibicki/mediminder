package mediminder.com.mediminder;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.widget.Toast;

@SuppressWarnings("deprecation")
public class Settings extends PreferenceActivity implements SharedPreferences.OnSharedPreferenceChangeListener
{

    private String password;
    private String disease;
    private String allergies;
    private String bloodType;
    DatabaseHandler db;

    private BroadcastReceiver broadcastReceiver;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        Preference myPref = (Preference) findPreference("pref_logout");
        myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            public boolean onPreferenceClick(Preference preference) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Settings.this);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("id", -1);
                editor.apply();

                stopAlarms();

                Intent backToLoginActivity = new Intent(Settings.this, LoginActivity.class);
                startActivity(backToLoginActivity);

                stopAllActivities();

                return true;
            }
        });
    }

    private void stopAllActivities() {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.mediminder.com.mediminder.ACTION_LOGOUT");
        sendBroadcast(broadcastIntent);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sp, String key) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        int id = preferences.getInt("id", 1);

        try {
            db = new DatabaseHandler(getApplicationContext());
            db.open();
        }
        catch(Exception e){
            Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
        }
        try {
            if(key.equals("pref_password_change"))
            {
                password = preferences.getString("pref_password_change", "def");
                db.changePassword(id, password);
            }
            else if(key.equals("pref_disease_change"))
            {
                disease = preferences.getString("pref_disease_change", " ");
                db.fillUserIllness(id, disease);
            }
            else if(key.equals("pref_allergies_change"))
            {
                allergies = preferences.getString("pref_allergies_change", " ");
                db.fillUserAllergies(id, allergies);
            }
            else if(key.equals("pref_blood_type_change"))
            {
                bloodType = preferences.getString("pref_blood_type_change", " ");
                db.fillUserBloodType(id, bloodType);
            } if(key.equals("pref_sync")){
                stopAlarms();
            }
        }
        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Błąd przy aktualizacji profilu!", Toast.LENGTH_SHORT).show();
            System.out.println(e.toString());
        }
        finally{db.close();}
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onPause() {
        getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
        super.onPause();
    }

    private void stopAlarms() {
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        alarmReceiver.stopDailyScheduling(this);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
