package mediminder.com.mediminder;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * @author Mikołaj Antczak
 *
 * Obsluga ekranu listy przypomnien
 * Jest to konkretna strona ViewPager z Home.java
 */
public class ReminderListPage extends Fragment {

    /**
     * Klasa zarzadzajaca baza danych
     */
    private DatabaseHandler databaseHandler;

    /**
     * Tryb widoku: 0 - dzienny, 1 - tygodniowy
     */
    private int mode;
    /**
     * Poczatkowa wyswietlana data
     */
    private String startDate;
    /**
     * Koncowa wyswietlana data
     */
    private String endDate;

    /**
     * Wyswietla informacje jesli lista jest pusta
     */
    private RelativeLayout infoLayout;
    /**
     * Ikona ladowania przypomnien
     */
    private ProgressBar progressBar;
    /**
     * Widok listy przypomnien
     */
    private ListView cardList;
    /**
     * Adapter obslugujacy elementy widoku listy przypomnien
     */
    private CardAdapter cardAdapter;
    /**
     * Lista przypomnien
     */
    private ArrayList<ReminderItem> reminderItems;

    /**
     * Okresla argument trybu widoku
     */
    public static final String MODE = "mode";
    /**
     * Określa argument poczatkowej daty
     */
    public static final String DATE_START = "dateStart";
    /**
     * Okresla argument koncowej daty
     */
    public static final String DATE_END = "dateEnd";

    /**
     *
     * @param startDate Data poczatkowa
     * @return Nowa instancja Fragmentu w przypadku widoku dziennego
     */
    public static ReminderListPage newInstance(String startDate) {
        ReminderListPage fragment = new ReminderListPage();
        Bundle args = new Bundle();
        args.putInt(MODE, 0);
        args.putString(DATE_START, startDate);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     *
     * @param startDate Data poczatkowa
     * @param endDate Data koncowa
     * @return Nowa instancja Fragmentu w przypadku widoku tyognidowego
     */
    public static ReminderListPage newInstance(String startDate, String endDate) {
        ReminderListPage fragment = new ReminderListPage();
        Bundle args = new Bundle();
        args.putInt(MODE, 1);
        args.putString(DATE_START, startDate);
        args.putString(DATE_END, endDate);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * Tworzy Fragment
     *
     * @param savedInstanceState Trzyma informaje o stanie Fragmentu
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mode = getArguments().getInt(MODE);
        startDate = getArguments().getString(DATE_START);
        if(mode == 0)
            endDate = getArguments().getString(DATE_START);
        else
            endDate = getArguments().getString(DATE_END);
    }

    /**
     * Buduje widok Fragmentu
     *
     * @param inflater
     * @param container
     * @param savedInstanceState Trzyma informaje o stanie Fragmentu
     * @return Glowny widok Fragmentu
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(
                R.layout.reminder_list_page, container, false);

        infoLayout = (RelativeLayout) rootView.findViewById(R.id.info_layout);
        progressBar = (ProgressBar) rootView.findViewById(R.id.progressBarLoading);

        cardList = (ListView) rootView.findViewById(R.id.remind_list);
        if(mode == 0)
            initDayMode();
        else
            initWeekMode();

        //jeśli w wyświetlanym czasie jest obecna data, zescrolluj do niej
        String currentStringDate = Common.currentStringDate();
        if((endDate != null && endDate.compareTo(currentStringDate) >= 0 && startDate.compareTo(currentStringDate) <= 0) || startDate.equals(currentStringDate)){
            int currentSelection = -1;
            if(getActivity().getIntent().hasExtra("userReminderId"))
                 currentSelection = findPositionByReminderId(getActivity().getIntent().getIntExtra("userReminderId", -1));

            if(currentSelection == -1)
                currentSelection = findCurrentReminder();

            cardList.setSelection(currentSelection);
        }

        return rootView;
    }

    /**
     * Znajduje wprowadzone przypomnienie po ID
     * W przypadku znalezienia ustawia je jako rozwiniete na widoku listy, odswieza liste i zwraca pozycje
     * W przypadku nieznalezenia zwraca wartosc -1
     *
     * @param reminderId ID przypomnienia
     * @return Pozycja przypomnienia na liscie
     */
    private int findPositionByReminderId(int reminderId) {
        for(int i = 0; i < reminderItems.size(); i++) {
            ReminderItem item = reminderItems.get(i);
            if (item.getReminderId() == reminderId) {
                item.setExpanded(true);
                cardAdapter.notifyDataSetChanged();
                return i;
            }
        }
        return -1;
    }

    /**
     * Inicjuje widok tygodniowy
     */
    private void initWeekMode() {

    }

    /**
     * Inicjuje widok tydgodniowy
     * Rozpoczyna ladowanie przypomnien do listy
     * Ukrywa instrukcje obslugi
     * Dodaje adapter do listy wraz z elementami
     */
    public void initDayMode() {
        reminderItems = loadDataForAdapter();
        if(reminderItems.isEmpty())
            infoLayout.setVisibility(View.VISIBLE);

        cardAdapter = new CardAdapter(this, R.layout.remind_list_item, reminderItems);
        cardList.setAdapter(cardAdapter);
    }

    /**
     * Ladowanie z bazy listy przypomnien
     *
     * @return Lista przypomnien
     */
    private ArrayList<ReminderItem> loadDataForAdapter() {

        try {
            databaseHandler = new DatabaseHandler(getActivity().getApplicationContext());
            databaseHandler.open();
            //na ewentualna przyszlosc: do widoku tyogniowego trzeba pobierac do pillitemu liste
            // statusow pomiedzy startdate a end-date i potem dla kazdego dnia mozna wyswietlac odpowiedni status
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity().getApplicationContext());
            return databaseHandler.getUserReminders(sharedPreferences.getInt("id", 1), startDate, endDate, startDate);
        } catch (Exception e) {
            //wyświetlanie komunikatu o błędzie ładowania
        } finally {
            progressBar.setVisibility(View.GONE);
            databaseHandler.close();
        }

        return new ArrayList<ReminderItem>();
    }

    /**
     *
     * @return Pozycja aktualnego (pod wzgledem czasu) przypomnienia
     */
    private int findCurrentReminder() {
        int currentSelection = 0;
        boolean foundCurrent = false;
        for(int i = 0; i < reminderItems.size() && !foundCurrent; i++){
            String currentStringDate = Common.currentStringDate();
            if((reminderItems.get(i).getDateStart().compareTo(currentStringDate) <= 0 &&
                    reminderItems.get(i).getDateEnd().compareTo(currentStringDate) >= 0 &&
                    reminderItems.get(i).getTime().compareTo(currentTime()) >= 0)) {
                currentSelection = i;
                foundCurrent = true;
            }
        }
        return currentSelection;
    }

    /**
     *
     * @return Aktualny czas
     */
    private String currentTime() {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
        return dateFormat.format(new Date());
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public ListView getCardList() {
        return cardList;
    }
    public ArrayList<ReminderItem> getReminderItems() {
        return reminderItems;
    }

    public RelativeLayout getInfoLayout() {
        return infoLayout;
    }
}
