package mediminder.com.mediminder;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Basia on 2014-11-19.
 */
public class MedChoiceItem implements Parcelable {

    private int actualAmount;
    private int userMedicineReminderId;
    private int userMedicineId;
    private String name;
    private int type;
    private int color;
    private int dosage;
    private String unit;

    /**
     *
     * @param userMedicineId id
     * @param name nazwa
     * @param type rodzaj leku
     * @param color
     * @param unit jednostka
     */
public MedChoiceItem(int userMedicineId, String name, int type, int color, String unit, int actualAmount) {
    this.userMedicineId = userMedicineId;
    this.name = name;
    this.type = type;
    this.color = color;
    this.unit = unit;
    this.actualAmount = actualAmount;
}
    private MedChoiceItem(Parcel in) {
        userMedicineReminderId = in.readInt();
        userMedicineId = in.readInt();
        name = in.readString();
        type = in.readInt();
        color = in.readInt();
        dosage = in.readInt();
        unit = in.readString();
        actualAmount = in.readInt();
    }

    public void writeToParcel(Parcel out, int flags) {
        out.writeInt(userMedicineReminderId);
        out.writeInt(userMedicineId);
        out.writeString(name);
        out.writeInt(type);
        out.writeInt(color);
        out.writeInt(dosage);
        out.writeString(unit);
        out.writeInt(actualAmount);

    }


    public int describeContents() {
        return 0;
    }

    // Just cut and paste this for now
    public static final Parcelable.Creator<MedChoiceItem> CREATOR = new Parcelable.Creator<MedChoiceItem>() {
        public MedChoiceItem createFromParcel(Parcel in) {
            return new MedChoiceItem(in);
        }

        public MedChoiceItem[] newArray(int size) {
            return new MedChoiceItem[size];
        }
    };

    public int getUserMedicineReminderId() {
        return userMedicineReminderId;
    }

    public void setUserMedicineReminderId(int userMedicineReminderId) {
        this.userMedicineReminderId = userMedicineReminderId;
    }

    public int getUserMedicineId() {
        return userMedicineId;
    }

    public void setUserMedicineId(int userMedicineId) {
        this.userMedicineId = userMedicineId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public String getUnit() {    return unit;}

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getActualAmount() {
        return actualAmount;
    }

    public void setActualAmount(int actualAmount) {
        this.actualAmount = actualAmount;
    }
}
