package mediminder.com.mediminder.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import mediminder.com.mediminder.CardAdapter;
import mediminder.com.mediminder.R;

/**
 * Created by Marcin on 2014-10-30.
 */
public class ReminderDeleteDialog extends DialogFragment {

    private CardAdapter cardAdapter;
    private int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.medi_delete_dialog, null);
        builder.setView(dialogView)
                .setPositiveButton("Usuń", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        cardAdapter.usunPrzypomnienie(position);
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        // Create the AlertDialog object and return it

        TextView przypoTytul = (TextView) dialogView.findViewById(R.id.przypoTytul);
        przypoTytul.setText("Czy usunąć wybrane przypomnienie?");
        return builder.create();
    }

    public void setMediListAndSelectedItemPosition(CardAdapter cardAdapter, int position) {
        this.cardAdapter = cardAdapter;
        this.position = position;
    }


}