package mediminder.com.mediminder;

import android.app.Activity;
//import android.support.v7.app.ActionBarActivity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class UserProfile extends Activity
{
    private EditText diseases;
    private EditText allergies;
    private RadioGroup bloodType;
    private RadioButton radioBtn1;
    private RadioButton radioBtn2;
    private RadioButton radioBtn3;
    private RadioButton radioBtn4;
    private RadioButton radioBtn5;
    private RadioButton radioBtn6;
    private RadioButton radioBtn7;
    private RadioButton radioBtn8;
    private Button next;

    public DatabaseHandler db;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        diseases = (EditText)findViewById(R.id.diseases);

        allergies = (EditText)findViewById(R.id.allergies);

        bloodType = (RadioGroup)findViewById(R.id.bloodType);

        radioBtn1 = (RadioButton)findViewById(R.id.radioGroupButton1);
        radioBtn2 = (RadioButton)findViewById(R.id.radioGroupButton2);
        radioBtn3 = (RadioButton)findViewById(R.id.radioGroupButton3);
        radioBtn4 = (RadioButton)findViewById(R.id.radioGroupButton4);
        radioBtn5 = (RadioButton)findViewById(R.id.radioGroupButton5);
        radioBtn6 = (RadioButton)findViewById(R.id.radioGroupButton6);
        radioBtn7 = (RadioButton)findViewById(R.id.radioGroupButton7);
        radioBtn8 = (RadioButton)findViewById(R.id.radioGroupButton8);

        next = (Button)findViewById(R.id.nextButton);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String choroby = diseases.getText().toString();
                String alergie = allergies.getText().toString();


                if(!(radioBtn1.isChecked()) && !(radioBtn2.isChecked()) && !(radioBtn3.isChecked()) &&
                !(radioBtn4.isChecked()) && !(radioBtn5.isChecked()) && !(radioBtn6.isChecked()) &&
                        !(radioBtn7.isChecked()) && !(radioBtn8.isChecked())) {
                    Toast.makeText(getApplicationContext(), "Nie podano grupy krwi!", Toast.LENGTH_SHORT).show();
                }
                else {

                    int selected = bloodType.getCheckedRadioButtonId();
                    RadioButton b = (RadioButton)findViewById(selected);
                    String grupaKrwi = b.getText().toString();

                try {
                    db = new DatabaseHandler(getApplicationContext());
                    db.open();
                }
                catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
                }

                try
                {
                    SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    db.fillUserProfile(sharedPreferences.getInt("id", 1), choroby, alergie, grupaKrwi);
                    /*String[] tmp = new String[6];
                    tmp= db.getUserProfileData(id);
                    System.out.println(tmp[0]);
                    System.out.println(tmp[1]);
                    System.out.println(tmp[2]);
                    System.out.println(tmp[3]);
                    System.out.println(tmp[4]);
                    System.out.println(tmp[5]);*/

                    Toast.makeText(getApplicationContext(), "Zaktualizowano profil!", Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(UserProfile.this, TutorialDemo.class);
                    startActivity(i);
                }
                catch(Exception e)
                {
                    Toast.makeText(getApplicationContext(), "Błąd przy aktualizacji profilu!", Toast.LENGTH_SHORT).show();
                    System.out.println(e.toString());
                }
                finally{db.close();}
            }}
        });
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(UserProfile.this, TutorialDemo.class);
        startActivity(i);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
