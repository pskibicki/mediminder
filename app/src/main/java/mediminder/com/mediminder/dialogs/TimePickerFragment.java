package mediminder.com.mediminder.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.widget.EditText;
import android.widget.TimePicker;

import java.util.Calendar;

/**
 * Created by Basia on 2014-11-10.
 */
public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private EditText editText;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), this, hour, minute,
                DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if((hourOfDay<10) && (minute<10)) {
            editText.setText("0"+hourOfDay + ":0" + minute);
        } else if (hourOfDay<10) {
            editText.setText("0"+hourOfDay + ":" + minute);
        } else if(minute<10) {
            editText.setText(hourOfDay + ":0" + minute);
        }
        else {editText.setText(hourOfDay + ":" + minute);}
    }


    public void setText(EditText newEditText) {
        this.editText = newEditText;
    }
}
