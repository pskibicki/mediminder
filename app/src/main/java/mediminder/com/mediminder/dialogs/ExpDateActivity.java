package mediminder.com.mediminder.dialogs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

import mediminder.com.mediminder.Home;
import mediminder.com.mediminder.R;

/**
 * Dialog przypomnienia o kończącej się dacie ważności leku.
 * Created by Basia on 2014-12-09.
 */
public class ExpDateActivity extends Activity {

    private Button positiveButton;
    private Button negativeButton;
    private static final int DELAY = 1;
    private ArrayList<String> expMedsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.exp_date_reminder);

        Intent intent = getIntent();
        expMedsList= intent.getStringArrayListExtra("expList");


        String str = "Kończą się terminy ważności następujących leków: \n";
        for (int i = 0; i < expMedsList.size(); i++) {
            str = str + expMedsList.get(i) + "\n";
        }

        TextView text = (TextView) findViewById(R.id.text);
        text.setText(str);


        positiveButton = (Button) findViewById(R.id.positive_button);
        negativeButton = (Button) findViewById(R.id.negative_button);

        positiveButton.setText("Ok");
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        negativeButton.setText("Przypomnij później");
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                delayDialog();
            }
        });
    }


    @Override
    public void onBackPressed(){
    }

    private void delayDialog() {
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ExpDateActivity.this.getApplicationContext(), ExpDateActivity.class);
        intent.putExtra("expList", expMedsList);
        PendingIntent alarmIntent = PendingIntent.getActivity(ExpDateActivity.this.getApplicationContext(), 1, intent, PendingIntent.FLAG_ONE_SHOT);
        if(Build.VERSION.SDK_INT != 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        finish();
    }





}
