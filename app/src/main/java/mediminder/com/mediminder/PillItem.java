package mediminder.com.mediminder;

/**
 * Created by Mikołaj on 2014-10-11.
 */
public class PillItem {

    private int medicineReminderID;
    private String name;
    private int type;
    private int color;
    private int dosage;
    private boolean selected;
    private boolean confirmed;
    private boolean afterAnimation = true;
    private int userMedicineId;

    public PillItem(int medicineReminderID, String name, int type, int color, int dosage, boolean confirmed, int userMedicineId){
        this.medicineReminderID = medicineReminderID;
        this.name = name;
        this.type = type;
        this.color = color;
        this.dosage = dosage;
        this.confirmed = confirmed;
        this.selected = confirmed;
        this.userMedicineId = userMedicineId;
    }

    public int getMedicineReminderID() {
        return medicineReminderID;
    }

    public void setMedicineReminderID(int medicineReminderID) {
        this.medicineReminderID = medicineReminderID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int image) {
        this.type = image;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getDosage() {
        return dosage;
    }

    public void setDosage(int dosage) {
        this.dosage = dosage;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean isConfirmed() {
        return confirmed;
    }

    public void setConfirmed(boolean confirmed) {
        this.confirmed = confirmed;
    }

    public boolean isAfterAnimation() {
        return afterAnimation;
    }

    public void setAfterAnimation(boolean afterAnimation) {
        this.afterAnimation = afterAnimation;
    }

    public int getUserMedicineId() {
        return userMedicineId;
    }

    public void setUserMedicineId(int userMedicineId) {
        this.userMedicineId = userMedicineId;
    }


}
