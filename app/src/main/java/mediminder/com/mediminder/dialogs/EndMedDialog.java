package mediminder.com.mediminder.dialogs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import mediminder.com.mediminder.Home;
import mediminder.com.mediminder.R;

/**
 * Created by Basia on 2015-01-19.
 */
public class EndMedDialog extends DialogFragment {
//    private Button positiveButton;
//    private Button negativeButton;
    String names;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.end_med_reminder);
//
//        Intent intent = getIntent();
//        names = intent.getStringExtra("names");
//
//        String str = "Kończą się następujące leki:" + names+"!";
//        TextView text = (TextView) findViewById(R.id.text);
//        text.setText(str);
//
//
//        positiveButton = (Button) findViewById(R.id.positive_button);
////        negativeButton = (Button) findViewById(R.id.negative_button);
//
//        positiveButton.setText("Ok");
//        positiveButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                finish();
//            }
//        });
////        negativeButton.setText("Przypomnij później");
////        negativeButton.setOnClickListener(new View.OnClickListener() {
////            @Override
////            public void onClick(View v) {
////                finish();
////            }
////        });
//    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.end_med_dialog, null);
        builder.setView(dialogView)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        // Create the AlertDialog object and return it

        names = getArguments().getString("names");

        String str = "Kończą się następujące leki:" + names+"!";

        TextView text = (TextView) dialogView.findViewById(R.id.info);
        text.setText(str);
        return builder.create();
    }


}
