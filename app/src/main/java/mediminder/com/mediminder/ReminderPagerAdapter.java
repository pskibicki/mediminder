package mediminder.com.mediminder;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author Mikołaj Antczak
 *
 * Klasa adaptera ViewPager odpowiadajaca za ladowanie sie stron na liscie przypomnien
 */
public class ReminderPagerAdapter extends FragmentStatePagerAdapter {

    /**
     * Zapisuje aktualnie zaladowane strony
     */
    public Map<Integer, ReminderListPage> mPageReferenceMap;

    /**
     * Tryb dzialania listy przypomnien: 0 - widok dzienny, 1 - widok tygodniowy
     */
    private int mode; //0 - day, 1 - week
    /**
     * Poczatkowa data wyswietlanej aktualnie strony
     */
    private String currentPageStartDate;
    /**
     * Koncowa data wyswietlanej aktualnie strony
     */
    private String currentPageEndDate;

    /**
     * Inicjuje adapter
     *
     * @param supportFragmentManager menadzer fragmentow ktory laduje odpowiednie strony (Fragmenty)
     */
    public ReminderPagerAdapter(FragmentManager supportFragmentManager) {
        super(supportFragmentManager);
        mPageReferenceMap = new HashMap<>();
    }

    /**
     * Laduje nowa strone
     *
     * @param position pozycja aktualnie wyswietlanej strony liczona od 01.01.1970
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        ReminderListPage pageFragment;
        if(mode == 0)
            pageFragment = ReminderListPage.newInstance(getStringDateFromPosition(position));
        else
            pageFragment =  ReminderListPage.newInstance(currentPageStartDate, currentPageEndDate); // TODO: wyliczanie end date na podstawie add day (7 dni...)

        mPageReferenceMap.put(Integer.valueOf(position), pageFragment);
        return pageFragment;
    }

    /**
     *
     * @return Liczba stron
     */
    @Override
    public int getCount() {
        try {
            String endDate = "2099-12-31";
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
            Date date = dateFormat.parse(endDate);
            return (int) TimeUnit.DAYS.convert(date.getTime(), TimeUnit.MILLISECONDS);
        } catch (ParseException e) {
            e.printStackTrace();
            return 1;
        }
    }

    /**
     * Konczy dzialanie strony jesli ta nie miesci sie juz w buforze
     *
     * @param container
     * @param position pozycja strony konczacej dzialanie
     * @param object
     */
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        super.destroyItem(container, position, object);
        mPageReferenceMap.remove(Integer.valueOf(position));
    }

    /**
=     *
     * @param key numer strony
     * @return Wybrany Fragment - strona z HashMapy trzymajacej aktualnie zaladowane strony
     */
    public ReminderListPage getFragment(int key) {

        return mPageReferenceMap.get(key);
    }
    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getCurrentPageStartDate() {
        return currentPageStartDate;
    }

    public void setCurrentPageStartDate(String currentPageStartDate) {
        this.currentPageStartDate = currentPageStartDate;
    }

    public String getCurrentPageEndDate() {
        return currentPageEndDate;
    }

    public void setCurrentPageEndDate(String currentPageEndDate) {
        this.currentPageEndDate = currentPageEndDate;
    }

    /**
     *
     * @param position Pozycja strony
     * @return Data liczona na podstawie pozycji strony
     */
    public String getStringDateFromPosition(int position){
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        Date selectedDate = Common.addDays(new Date(0), position);
        return dateFormat.format(selectedDate);
    }
}
