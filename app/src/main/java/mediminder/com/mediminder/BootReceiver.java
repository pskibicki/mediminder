package mediminder.com.mediminder;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * @author Mikołaj Antczak
 *
 */
public class BootReceiver extends BroadcastReceiver {

    /**
     * Akcja ktora pozwala uruchomic scheduling alarmów po restrcie telefonów
     */
    private final String BOOT_ACTION = "android.intent.action.BOOT_COMPLETED";

    public BootReceiver() {
    }

    /**
     * Rozpoczyna dzialanie po otrzymaniu zadania
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equalsIgnoreCase(BOOT_ACTION)) {
            // Wykrywanie zakończenia rozruchu i uruchamianie usługi
            startAlarmReceiver(context);
        }
    }

    /**
     * Inicjuje ladowanie dziennych alarmow i scheduling dobowy
     *
     * @param context
     */
    private void startAlarmReceiver(Context context) {
        AlarmReceiver alarmReceiver = new AlarmReceiver();

        alarmReceiver.setDailyAlarms(context);
        alarmReceiver.setDailyScheduling(context);
    }
}
