package mediminder.com.mediminder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Locale;

/**
 * Klasa LoginAcitvity tworzy ekran logowania.
 */
public class LoginActivity extends Activity
{
    private Button login_button;
    private Button register_button;
    private EditText email;
    private EditText password;
    public DatabaseHandler databaseHandler;

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;

    SharedPreferences preferences;

    /**
     * W funkcji onCreate tworzony jest layout ekranu.
     * Do przycisku login_button podpięta jest akcja polegająca na sprawdzeniu poprawności wpisanych danych.
     * Wykorzystuje do tego celu obiekt klasy DatabaseHandler i funcje: checkIfExists, getPassword oraz getUserID.
     * Przycisk register_button przenosi do ekranu rejestracji.
     * @param savedInstanceState
     */
    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        if(preferences.getInt("id", -1) != -1){
            startHome();
        }

        setContentView(R.layout.activity_login);

        email = (EditText)findViewById(R.id.email);
        password = (EditText)findViewById(R.id.password);

        login_button = (Button)findViewById(R.id.login_button);
        login_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String login_EditText = email.getText().toString();
                String password_EditText = password.getText().toString();

                try {
                    databaseHandler = new DatabaseHandler(getApplicationContext());
                    databaseHandler.open();
                }
                catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
                }

                try{
                    int loginExistence = databaseHandler.checkIfExists(login_EditText);
                    switch(loginExistence){
                        case 0:
                            Toast.makeText(getApplicationContext(), "Podany login nie istnieje!", Toast.LENGTH_SHORT).show();
                            break;
                        case 1:
                            String password_DB = databaseHandler.getPassword(login_EditText);
                            int id = databaseHandler.getUserID(login_EditText);
                            if(password_DB.equals(password_EditText))
                            {

                                if(preferences.getInt("id", -1) == -1 && preferences.getBoolean("pref_sync", true))
                                    startScheduling();

                                SharedPreferences.Editor editor = preferences.edit();
                                editor.putInt("id", id);
                                editor.apply();

                                startHome();
                            }
                            else
                            {
                                Toast.makeText(getApplicationContext(), "Błędne hasło!", Toast.LENGTH_SHORT).show();
                            }
                            break;
                        default:
                            break;
                    }
                    databaseHandler.close();


                }
                catch(Exception e){System.out.println(e.toString());}
                finally {
                    databaseHandler.close();
                }
            }
        });

        register_button = (Button)findViewById(R.id.register_button);
        register_button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, RegisterForm.class);
                startActivity(intent);
            }
        });
    }

    /**
     * Przeniesienie do ekranu głównego aplikacji.
     */
    private void startHome() {
        Intent intent = new Intent(LoginActivity.this, Home.class);
        startActivity(intent);
        finish();
    }

    /**
     * Włącza dobowe ładowanie alarmów.
     */
    private void startScheduling() {
        AlarmReceiver alarmReceiver = new AlarmReceiver();
        alarmReceiver.setDailyScheduling(this);
    }
}
