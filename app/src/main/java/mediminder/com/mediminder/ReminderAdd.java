package mediminder.com.mediminder;

import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import mediminder.com.mediminder.dialogs.ChosenMediDeleteDialog;
import mediminder.com.mediminder.dialogs.DatePickerFragment;
import mediminder.com.mediminder.dialogs.FoodReminderActivity;
import mediminder.com.mediminder.dialogs.NoteDialog;
import mediminder.com.mediminder.dialogs.ReminderDialogActivity;
import mediminder.com.mediminder.dialogs.TimePickerFragment;

/**
 * Created by Basia on 2014-11-09.
 */
public class ReminderAdd extends ActionBarActivity {

    private EditText medicineEditText;
    private EditText unitEditText;
    private EditText timeEditText;
    private EditText startEditText;
    private EditText stopEditText;
    private EditText doseEditText;
    private Button addButton;
    private Button medicineButton;
    private DatabaseHandler databaseHandler;
    private ListView choiceList;
    private ArrayList<MedChoiceItem> items;
    private ArrayList<Integer> selectedUserMedicinesID;
    private MedChoiceAdapter adapter;
    private TextView noteTextView;
    private String notatka;
    private AlarmManager alarmMgr;

    private HashMap<Integer, String> rodzajeJednostek;
    int wybranaJednostka;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminder_add);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        medicineEditText = (EditText) findViewById(R.id.medicineEditText);
        timeEditText = (EditText) findViewById(R.id.timeEditText);
        startEditText = (EditText) findViewById(R.id.startEditText);
        stopEditText = (EditText) findViewById(R.id.stopEditText);
        addButton = (Button) findViewById(R.id.addButton);
        doseEditText = (EditText) findViewById(R.id.doseEditText);
        medicineButton = (Button) findViewById((R.id.medicineButton));
        choiceList = (ListView) findViewById(R.id.choiceList);


        selectedUserMedicinesID = new ArrayList<Integer>();
        items = new ArrayList<MedChoiceItem>();

        rodzajeJednostek = new HashMap<Integer, String>();
        rodzajeJednostek.put(R.drawable.tab_zwykla, "szt");
        rodzajeJednostek.put(R.drawable.tab_dluga2,"szt" );
        rodzajeJednostek.put(R.drawable.tab_dzielona,"szt" );
        rodzajeJednostek.put(R.drawable.tab_dluga1,"szt" );
        rodzajeJednostek.put(R.drawable.tab_syrop,"ml");
        rodzajeJednostek.put(R.drawable.tab_strzykawka,"ml" );
        rodzajeJednostek.put(R.drawable.tab_dluga3,"szt" );
        rodzajeJednostek.put(R.drawable.tab_kropla,"szt" );

        timeEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerFragment timePicker = new TimePickerFragment();
                timePicker.setText(timeEditText);
                timePicker.show(getFragmentManager(), "timeEditText");
            }
        });

        startEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        //tutaj dodstajmy wybraną datę. uwaga miesiące w formacie 0-11
                        monthOfYear = monthOfYear + 1;
                        startEditText.setText(dayOfMonth+ "." + monthOfYear + "." + year);
                    }
                };
                Time t = new Time();
                t.setToNow();
                String year = String.valueOf(t.year);
                String month = (t.month+1) < 10 ? 0+ String.valueOf(t.month+1) : String.valueOf(t.month+1);
                String day = t.monthDay < 10 ? 0 + String.valueOf(t.monthDay) : String.valueOf(t.monthDay);
                String currentDate = day + "." + month + "." + year;
                int currentYear = Integer.valueOf(year);
                int currentMonth = Integer.valueOf(month) - 1;
                int currentDay = Integer.valueOf(day);

                DatePickerDialog dpd = new DatePickerDialog(ReminderAdd.this, onDateSetListener, currentYear, currentMonth, currentDay);
                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
                Date d = null;
                try {
                    d = sdf.parse(currentDate);
                    dpd.getDatePicker().setMinDate(d.getTime());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                dpd.show();
            }
        });

        stopEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerFragment rd = new DatePickerFragment();
                rd.setEditText4(stopEditText);
                rd.show(getFragmentManager(), "stopEditText");
            }
        });

        noteTextView = (TextView) findViewById(R.id.noteTextView);

        noteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NoteDialog rd = new NoteDialog();
                rd.setReminderAdd(ReminderAdd.this);
                rd.show(getFragmentManager(), "notatka");
            }
        });
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent resultIntent = new Intent();
                String time = timeEditText.getText().toString();
                String startDate = startEditText.getText().toString();
                String stopDate = stopEditText.getText().toString();
                try {
                    DateFormat originalFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
                    DateFormat targetFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    Date start= originalFormat.parse(startDate);
                    startDate = targetFormat.format(start);

                    Date stop= originalFormat.parse(stopDate);
                    stopDate = targetFormat.format(stop);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if(timeEditText.getText().toString().isEmpty() || stopEditText.getText().toString().isEmpty() ||
                        startEditText.getText().toString().isEmpty() ||

                        !sprawdzDawki() || selectedUserMedicinesID.isEmpty())
                    Toast.makeText(getBaseContext(), "Wprowadź wszystkie dane", Toast.LENGTH_SHORT).show();
                else if(!compareDates(startDate,stopDate)){
                    Toast.makeText(getBaseContext(), "Data rozpoczęcia musi być wcześniej od daty zakończenia.", Toast.LENGTH_SHORT).show();
                }
                else {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
                    Date currentDate = new Date();

                    long idPrzypomnienia = dodajPrzypomnienieDoBazy(time, startDate, stopDate, items, notatka);
                    if(idPrzypomnienia != -1) {

                        String currentDateString = Common.currentStringDate();
                        if (currentDateString.compareTo(startDate) >= 0 && currentDateString.compareTo(stopDate) <= 0)
                            addAlarmforReminder((int) idPrzypomnienia, time, selectedUserMedicinesID);

                        resultIntent.putExtra("ID", (int) idPrzypomnienia);
                        resultIntent.putParcelableArrayListExtra("key", items);
                        resultIntent.putExtra("time", time);
                        resultIntent.putExtra("startDate", startDate);
                        resultIntent.putExtra("stopDate", stopDate);
                        resultIntent.putExtra("note", notatka);

                        setResult(1, resultIntent);
                    }
                    finish();
                }
            }
        });

        medicineButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //medicineEditText.setText("Test");
                Intent intent = new Intent(getBaseContext(), ReminderChooseMed.class);
                intent.putIntegerArrayListExtra("selectedItems", selectedUserMedicinesID);
                startActivityForResult(intent, 0);
            }
        });

        choiceList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                ChosenMediDeleteDialog newFragment = new ChosenMediDeleteDialog();
                newFragment.setMediListAndSelectedItemPosition(ReminderAdd.this, position);
                newFragment.show(getFragmentManager(), "delete");

                return false;
            }
        });

    }

    /**
     * Sprawdzenie czy wpisane dawki są różne od 0.
     * @return true - dawki sa różne od zera,  false - dawka jest równa 0.
     */
    private boolean sprawdzDawki() {
        for(MedChoiceItem item : items)
            if(item.getDosage() == 0)
                return false;
        return true;
    }

    /**
     * Dodanie przypomnienia do bazy.
     * @param time godzina przypomnienia
     * @param startDate data rozpoczęcia przypomnienia
     * @param stopDate data zakończenia przypomnienia
     * @param items leki
     * @param note notatka
     * @return id przypomnienia
     */
    private long dodajPrzypomnienieDoBazy(String time, String startDate, String stopDate, ArrayList<MedChoiceItem> items, String note) {
        long idPrzypomnienia = -1;
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            idPrzypomnienia = (int) databaseHandler.addReminder(startDate, stopDate, time, items, sharedPreferences.getInt("id", 1), note); //dodany pusty string jako notatka
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }
        return idPrzypomnienia;
    }


    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){

        if(resultCode == 1){
            Bundle b = data.getExtras();
            int[] newSelectedUserMedicinesID = b.getIntArray("selectedItems");
            for(int id : newSelectedUserMedicinesID){
                selectedUserMedicinesID.add(id);
            }
            //medicineEditText.setText(medList.get(selectedUserMedicinesID[0]).getNazwa());
            items = getMedicines(selectedUserMedicinesID);
            adapter = new MedChoiceAdapter(this, R.layout.med_choice_item, items);

//            for(int i=0;i<selectedUserMedicinesID.length;i++) {
//                items.add(new MedChoiceItem(0, medList.get(selectedUserMedicinesID[i]).getNazwa(),"ml",medList.get(selectedUserMedicinesID[i]).getDawkaWJednejSztuce()));
//                adapter.notifyDataSetChanged();
//            }
            choiceList.setItemsCanFocus(true);
            //View footer = ((LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);

            //choiceList.addFooterView(footer);
            choiceList.setAdapter(adapter);

            updateListHeight();
            //doseEditText.setText(medList.get(selectedUserMedicinesID[0]).));

        }

    }

    private void updateListHeight() {
        ViewGroup.LayoutParams params = choiceList.getLayoutParams();
        if(adapter.getCount() > 0)
            params.height = dpToPx(48f)*choiceList.getCount() + choiceList.getDividerHeight()*(choiceList.getCount()-1);
        else
            params.height = 0;
        choiceList.setLayoutParams(params);
        choiceList.requestLayout();
    }


//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.reminder_add, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }

    /**
     * Porównanie dat - rozpoczęcia i zakończenia. Data rozpoczęcia nie może być później niż data zakończenia.
     * @param startDate data rozpoczęcia przypomnienia
     * @param stopDate data zakończenia przypomnienia
     * @return true - daty są w odpowiedniej kolejności, false - data rozpoczęcia jest po dacie przypomnienia
     */
    private boolean compareDates(String startDate, String stopDate) {
        boolean isOk = false;
        Date start;
        Date stop;
        try {
            start = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(startDate);
            stop = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).parse(stopDate);
            if((start.before(stop)) || (start.equals(stop))) {
                isOk = true;
            }

        } catch (ParseException e) {
            e.printStackTrace();

        }
        return isOk;
    }

    /**
     * Pobranie listy leków z bazy na podstawie listy id wybranych leków.
     * @param resultArr lista id wybranych leków
     * @return lista leków ze wszystkimi parametrami
     */
    private ArrayList<MediItem> getMedList(ArrayList<Integer> resultArr) {
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            return databaseHandler.getSelectedUserMedicines(resultArr, true);
        }
        catch(Exception e){
            System.out.println("Blad przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd pobierania danych z bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }
        return new ArrayList<MediItem>();
    }

    /**
     * Dodanie leków wybranych przez użytkownika.
     * @param resultArr lista id wybranych leków
     * @return lista leków z parametrami do wyśswietlenia na liście (nazwa, rodzaj, jednostka).
     */
    private ArrayList<MedChoiceItem> getMedicines(ArrayList<Integer> resultArr) {

        if(this.selectedUserMedicinesID.size()>0) {
            ArrayList<MedChoiceItem> list = new ArrayList<MedChoiceItem>();
            ArrayList<MediItem> medList = getMedList(resultArr);

            for(MediItem i : medList){
                list.add(new MedChoiceItem(i.getIdLekuUzytkownika(), i.getNazwa(), i.getRodzaj(), i.getKolor(), rodzajeJednostek.get(i.getRodzaj()), i.getAktualnaIloscSztuk()));
            }

            return list;
        } else {
            return new ArrayList<MedChoiceItem>();
        }

    }

    private int dpToPx(float dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.densityDpi / (float) DisplayMetrics.DENSITY_DEFAULT));
    }

    /**
     * Usunięcie leku z listy wybranych leków.
     * @param position pozycja leku wybranego do usunięcia
     */
    public void usunLek(int position) {
        adapter.remove(adapter.getItem(position));
        selectedUserMedicinesID.remove(position);
        updateListHeight();
        adapter.notifyDataSetChanged();
    }

    /**
     * Dodanie notatki do przypomnienia.
     * @param text tekst notatki
     */
    public void addNote(String text) {
        notatka = text;
    }

    public void addAlarmforReminder(int reminderId, String time, ArrayList<Integer> items) {

        Intent intent = new Intent(getApplicationContext(), ReminderDialogActivity.class);
        intent.putExtra("userReminderId", reminderId);
        PendingIntent alarmIntent = PendingIntent.getActivity(getApplicationContext(), reminderId, intent, 0);
        alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);

        boolean zal0 = false;
        boolean zal1 = false;
        boolean zal2 = false;

        for(Integer medItem : items) {

            ArrayList<Integer> idList = new ArrayList<>();
            idList.add(medItem);
            ArrayList<MediItem> medicineList = new ArrayList<>();

            try {
                databaseHandler = new DatabaseHandler(getApplicationContext());
                databaseHandler.open();
                medicineList = databaseHandler.getSelectedUserMedicines(idList, true);
            }
            catch(Exception e){
                System.out.println("Błąd przy otwieraniu bazy");
                Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
            }
            finally {
                databaseHandler.close();
            }

            int zalecenieZywieniowe = medicineList.get(0).getWybraneZalecenieZywieniowe();
            if((!zal0 && 0 == zalecenieZywieniowe) || (!zal1 && 1 == zalecenieZywieniowe) || (!zal2 && 2 == zalecenieZywieniowe)){
                Intent foodIntent = new Intent(getApplicationContext(), FoodReminderActivity.class);
                foodIntent.putExtra("foodId", medicineList.get(0).getWybraneZalecenieZywieniowe());
                PendingIntent  foodAlarmIntent = PendingIntent.getActivity(getApplicationContext(), reminderId, foodIntent, 0);

                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd" + "-" + "HH:mm", Locale.getDefault());
                Date itemDate = new Date();
                try {
                    itemDate = dateFormat.parse(Common.currentStringDate() + "-" + time);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                Calendar calendar = Calendar.getInstance(Locale.getDefault());
                calendar.setTime(itemDate);
                switch(medicineList.get(0).getWybraneZalecenieZywieniowe()) {
                    case 0:
                        calendar.add(Calendar.MINUTE,-30);
                        break;
                    case 1:
                        calendar.add(Calendar.MINUTE,-30);
                        break;
                    case 2:
                        calendar.add(Calendar.MINUTE,-30);
                        break;
                }
                calendar.set(Calendar.SECOND, 0);

                if(Build.VERSION.SDK_INT != 19)
                    alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), foodAlarmIntent);
                else
                    alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), foodAlarmIntent);

                if(zalecenieZywieniowe == 0)
                    zal0 = true;
                else if(zalecenieZywieniowe == 1)
                    zal1 = true;
                else if(zalecenieZywieniowe == 2)
                    zal2 = true;
            }


        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-" + "HH:mm", Locale.getDefault());
        Date itemDate = new Date();
        try {
            itemDate = dateFormat.parse(Common.currentStringDate() + "-" + time);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTime(itemDate);
        calendar.set(Calendar.SECOND, 0);


        if(Build.VERSION.SDK_INT != 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), alarmIntent);

    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
