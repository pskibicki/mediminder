package mediminder.com.mediminder;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.content.WakefulBroadcastReceiver;

import java.util.Calendar;
import java.util.Locale;

import mediminder.com.mediminder.dialogs.ReminderDialogActivity;

/**
 * @author Mikołaj Antczak
 *
 * Inicjuje dobowy scheduling przypomnien i ladowanie przypomnien z calego dnia
 */
public class AlarmReceiver extends WakefulBroadcastReceiver {

    /**
     * menadzer zapewniajacy alarmy
     */
    private AlarmManager alarmMgr;
    // The pending intent that is triggered when the alarm fires.
    private PendingIntent schedulingIntent;

    /**
     * Pozwala na zaladowanie alarmow na caly dzien poza dzialaniem aplikajci
     *
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        setDailyAlarms(context);
    }

    /**
     * Scheduluje dzienne ladowanie alarmow
     *
     * @param context
     */
    public void setDailyScheduling(Context context) {
        alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, AlarmReceiver.class);
        schedulingIntent = PendingIntent.getBroadcast(context, 0, intent, 0);

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(System.currentTimeMillis());
        // Set the alarm's trigger time to 8:30 a.m.
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 30);
        calendar.set(Calendar.SECOND, 0);

        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, schedulingIntent);

        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);

    }

    /**
     * Ustawia alarmy na caly dzien za pomoca Service
     *
     * @param context
     */
    public void setDailyAlarms(Context context) {
        Intent service = new Intent(context, AlarmSchedulingService.class);

        // Start the service, keeping the device awake while it is launching.
        startWakefulService(context, service);
    }

    /**
     * Wylacza dobowe ladowania alarmow
     *
     * @param context
     */
    public void stopDailyScheduling(Context context) {
        // If the alarm has been set, cancel it.
        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(schedulingIntent);

        Intent intent = new Intent(context, ReminderDialogActivity.class);
        PendingIntent alarmIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        alarmMgr.cancel(alarmIntent);

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, BootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
