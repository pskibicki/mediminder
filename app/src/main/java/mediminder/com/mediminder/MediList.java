package mediminder.com.mediminder;

import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.faizmalkani.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import mediminder.com.mediminder.dialogs.MediDeleteDialog;


public class MediList extends ActionBarActivity {

    private DatabaseHandler databaseHandler;
    private FloatingActionButton mFab;
    private ListView listView;
    private ArrayList<MediItem> listaLekow;
    private MediAdapter adapter;
    private TextView info;
    private ArrayList<Integer> idUsunietychLekow;

    private BroadcastReceiver broadcastReceiver;
    boolean rosnacoName = true;
    boolean rosnacoTime = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.medi_list);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        mFab = (FloatingActionButton)findViewById(R.id.add_button);
        info = (TextView) findViewById(R.id.info);

        listView = (ListView) findViewById(R.id.listView);

        listaLekow = pobierzListeLekow();
        if(listaLekow.isEmpty())
            info.setVisibility(View.VISIBLE);

        adapter = new MediAdapter(this,R.layout.medi_item, listaLekow);
        listView.setAdapter(adapter);
        mFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mediAddIntent = new Intent(getBaseContext(), MediAdd.class);
                startActivityForResult(mediAddIntent, 0);
            }
        });
        mFab.listenTo(listView);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                registerForContextMenu(listView);

                return false;
            }
        });
    }

    private ArrayList<MediItem> pobierzListeLekow() {
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            return databaseHandler.getAllUsersMedicines(sharedPreferences.getInt("id", -1));
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd pobierania danych z bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return new ArrayList<MediItem>();
    }

    public boolean usunLek(int position) {

        boolean deleted = false;
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            int id = adapter.getItem(position).getIdLekuUzytkownika();
            deleted = databaseHandler.deleteUsersMedicine(id);
            if(deleted){
                adapter.remove(adapter.getItem(position));
                adapter.notifyDataSetChanged();

                if(idUsunietychLekow == null)
                    idUsunietychLekow = new ArrayList<>();
                idUsunietychLekow.add(id);
            }
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd podczas usuwania leku", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return deleted;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.medi_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(this, Settings.class);
            startActivity(settingsIntent);
        } else if(id == R.id.action_sort_name){
                sortujPoNazwie(item);
        } else if(id == R.id.action_sort_date){
                sortujPoDacie(item);
        }

        return super.onOptionsItemSelected(item);
    }

    private void sortujPoDacie(MenuItem menuItem) {
        Collections.sort(listaLekow, new Comparator<MediItem>() {
            @Override
            public int compare(MediItem item1, MediItem item2) {
                if (rosnacoTime)
                    return item1.getDataWaznosci().compareToIgnoreCase(item2.getDataWaznosci());
                else
                    return item2.getDataWaznosci().compareToIgnoreCase(item1.getDataWaznosci());
            }
        });
        adapter.notifyDataSetChanged();
        if (rosnacoTime)
            menuItem.setIcon(getResources().getDrawable(R.drawable.time_desc));
        else
            menuItem.setIcon(getResources().getDrawable(R.drawable.time_asc));
        rosnacoTime = !rosnacoTime;
    }

    private void sortujPoNazwie(MenuItem menuItem) {
        Collections.sort(listaLekow, new Comparator<MediItem>() {
            @Override
            public int compare(MediItem item1, MediItem item2) {
                if (rosnacoName)
                    return item1.getNazwa().compareToIgnoreCase(item2.getNazwa());
                else
                    return item2.getNazwa().compareToIgnoreCase(item1.getNazwa());
            }
        });
        adapter.notifyDataSetChanged();
        if (rosnacoName)
            menuItem.setIcon(getResources().getDrawable(R.drawable.sort_name_desc));
        else
            menuItem.setIcon(getResources().getDrawable(R.drawable.sort_name_asc));
        rosnacoName = !rosnacoName;
    }

    @Override
    protected void onActivityResult (int requestCode, int resultCode, Intent data){

        if(resultCode == 1){
            String nazwa = data.getStringExtra("Nazwa");
            int rodzaj = data.getIntExtra("Rodzaj", -1);
            int kolor = data.getIntExtra("Color", -1);
            int iloscSztuk = data.getIntExtra("Ilosc", -1);
            int idLekuUzytkownika = data.getIntExtra("ID", -1);

            MediItem newItem = new MediItem(idLekuUzytkownika, nazwa, rodzaj, kolor, iloscSztuk);
            listaLekow.add(newItem);
            if(!listaLekow.isEmpty())
                info.setVisibility(View.GONE);
            adapter.notifyDataSetChanged();
        } else if(resultCode == 2){
            int iloscSztuk = data.getIntExtra("Ilosc", -1);
            int position = data.getIntExtra("position", -1);
            MediItem item = listaLekow.get(position);
            item.setAktualnaIloscSztuk(iloscSztuk);
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onBackPressed(){
        if(idUsunietychLekow != null)
            setResult(2, new Intent().putIntegerArrayListExtra("idUsuniete", idUsunietychLekow));
        finish();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, 0, 0, "Edytuj");
        menu.add(0, 1, 1, "Usuń");
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();

        switch (item.getItemId()){
            case 0:
                Intent mediAddIntent = new Intent(getBaseContext(), MediAdd.class);
                mediAddIntent.putExtra("idLekuUzytkownika", listaLekow.get(info.position).getIdLekuUzytkownika());
                mediAddIntent.putExtra("position", info.position);
                startActivityForResult(mediAddIntent, 1);
                break;
            case 1:
                MediDeleteDialog newFragment = new MediDeleteDialog();
                newFragment.setMediListAndSelectedItemPosition(MediList.this, info.position);
                newFragment.show(getFragmentManager(), "delete");
                break;
        }
        return true;
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
