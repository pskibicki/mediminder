package mediminder.com.mediminder;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.content.Intent;


public class RegisterForm extends Activity
{
    private Button register_button;
    private EditText login_editText;
    private EditText password_editText;

    public DatabaseHandler db;

    private BroadcastReceiver broadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_form);

        broadcastReceiver = Common.finishActivityWhenLogout(this);

        login_editText = (EditText)findViewById(R.id.login_editText);
        password_editText = (EditText)findViewById(R.id.password_editText);

        register_button = (Button)findViewById(R.id.register_button);

        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = login_editText.getText().toString();
                String password = password_editText.getText().toString();

                try {
                    db = new DatabaseHandler(getApplicationContext());
                    db.open();
                }
                catch(Exception e){
                    Toast.makeText(getApplicationContext(), "Błąd przy otwieraniu bazy!", Toast.LENGTH_SHORT).show();
                }
                if(login.trim().length() == 0 || password.trim().length() == 0)
                {
                    Toast.makeText(getApplicationContext(), "Nie uzupełniono wszystkich pól!", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    try {
                        int loginExistence = db.checkIfExists(login);
                        switch (loginExistence) {
                            case 0:
                                int id = (int) db.addUser(login, password);
                                Toast.makeText(getApplicationContext(), "Dodano usera!", Toast.LENGTH_SHORT).show();
//todo: alarmy
                                SharedPreferences idPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                                SharedPreferences.Editor editor = idPref.edit();
                                editor.putInt("id", id);
                                editor.apply();

                                Intent i = new Intent(RegisterForm.this, UserProfile.class);
                                startActivity(i);
                                break;
                            case 1:
                                Toast.makeText(getApplicationContext(), "Podany login istnieje! Proszę wybrać inny.", Toast.LENGTH_SHORT).show();
                                break;
                            default:
                                break;
                        }
                        db.close();
                    } catch (Exception e) {
                        System.out.println(e.toString());
                    } finally {
                        db.close();
                    }
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register_form, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        unregisterReceiver(broadcastReceiver);
    }
}
