package mediminder.com.mediminder;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Basia on 2014-11-19.
 */

public class MedChoiceAdapter extends ArrayAdapter<MedChoiceItem> {


    private final LayoutInflater mInflater;
    private int layoutResource;

    public MedChoiceAdapter(Context context, int resource, ArrayList<MedChoiceItem> objects) {
        super(context, resource, objects);
        this.layoutResource = resource;
        mInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(layoutResource, null);
            holder.unitTextView= (TextView) convertView.findViewById(R.id.unitTextView);
            holder.nameEditText= (EditText) convertView.findViewById(R.id.medicineEditText);
            holder.doseEditText = (EditText) convertView.findViewById(R.id.doseEditText);

            holder.doseEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    int getPosition = (Integer) holder.doseEditText.getTag();
                    MedChoiceItem item = getItem(getPosition);
                    try {
                        item.setDosage(Integer.valueOf(s.toString()));
                    } catch(NumberFormatException e){
                        item.setDosage(0);
                    }
                }
            });

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        MedChoiceItem item = getItem(position);
        holder.unitTextView.setText(item.getUnit());
        holder.nameEditText.setText(item.getName());
        holder.doseEditText.setTag(position);

        return convertView;
    }

    public static class ViewHolder{
        public TextView unitTextView;
        public EditText nameEditText;
        public EditText doseEditText;
    }
}


