package mediminder.com.mediminder.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.widget.DatePicker;
import android.widget.EditText;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by Marcin on 2014-11-03.
 */
public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private EditText edittext4;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        int year;
        int month;
        int day;
//        if(edittext4.getText().toString().isEmpty()){
            final Calendar c = Calendar.getInstance(Locale.getDefault());
            year = c.get(Calendar.YEAR);
            month = c.get(Calendar.MONTH);
            day = c.get(Calendar.DAY_OF_MONTH);
//        } else{
//           String date = edittext4.getText().toString();
//            year =  Integer.valueOf(date.substring(0,3));
//            month = date.
//        }


        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        edittext4.setText(day+"."+(month+1)+"."+year);
    }

    public void setEditText4(EditText editText4) {
        this.edittext4 = editText4;
    }

}
