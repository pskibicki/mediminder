package mediminder.com.mediminder.dialogs;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mediminder.com.mediminder.Common;
import mediminder.com.mediminder.DatabaseHandler;
import mediminder.com.mediminder.Home;
import mediminder.com.mediminder.MediAdapter;
import mediminder.com.mediminder.MediItem;
import mediminder.com.mediminder.PillItem;
import mediminder.com.mediminder.R;
import mediminder.com.mediminder.ReminderItem;


public class ReminderDialogActivity extends Activity {

    private ListView listView;
    private MediAdapter adapter;
    private ArrayList<MediItem> listaLekow;
    private DatabaseHandler databaseHandler;
    private String data;
//    private String czas;
    private Button positiveButton;
    private Button negativeButton;

    private static final int DELAY = 1;

    private int reminderId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
//                WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY,
//                WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH,
//                PixelFormat.TRANSLUCENT);
//        WindowManager wm = (WindowManager) getSystemService(WINDOW_SERVICE);
//        View convertView = getLayoutInflater().inflate(R.layout.activity_dialog, null);
//        wm.addView(convertView, params);


        setContentView(R.layout.activity_dialog);

        Intent intent = getIntent();

        data = Common.currentStringDate();

        reminderId = intent.getIntExtra("userReminderId", 1);
        ReminderItem przypomnienie = pobierzPrzypomnienie(reminderId);

        listaLekow = listaLekowMediItem(przypomnienie.getPills());
        adapter = new MediAdapter(this,R.layout.medi_item_remind_dialog, listaLekow);
        listView = (ListView) findViewById(R.id.przypolista);
        listView.setAdapter(adapter);
        TextView notatka = (TextView) findViewById(R.id.notatka);
        notatka.setText(przypomnienie.getNote());
        if(przypomnienie.getNote() == null) {
            View separator = findViewById(R.id.top_separator);
            separator.setVisibility(View.GONE);
            notatka.setVisibility(View.GONE);
        }

        positiveButton = (Button) findViewById(R.id.positive_button);
        negativeButton = (Button) findViewById(R.id.negative_button);

        positiveButton.setText("Przejdź do aplikacji");
        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReminderDialogActivity.this, Home.class);
                intent.putExtra("userReminderId", reminderId);
                stopAllActivities();
                startActivity(intent);
                finish();
            }
        });
        negativeButton.setText("Przypomnij później");
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            delayDialog();
            }
        });
    }

    @Override
    public void onBackPressed(){
    }

    private void stopAllActivities() {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction("com.mediminder.com.mediminder.ACTION_LOGOUT");
        sendBroadcast(broadcastIntent);
    }

    private void delayDialog() {
        AlarmManager alarmMgr = (AlarmManager)getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(ReminderDialogActivity.this.getApplicationContext(), ReminderDialogActivity.class);
        intent.putExtra("userReminderId", reminderId);
        PendingIntent alarmIntent = PendingIntent.getActivity(ReminderDialogActivity.this.getApplicationContext(), reminderId, intent, PendingIntent.FLAG_ONE_SHOT);
        if(Build.VERSION.SDK_INT < 19)
            alarmMgr.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        else
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + DELAY * 1000 * 10, alarmIntent);
        finish();
    }

    private ReminderItem pobierzPrzypomnienie(int reminderId) {
        ReminderItem przypomnienie;
        try {
            databaseHandler = new DatabaseHandler(getApplicationContext());
            databaseHandler.open();
            // tu funkcja która pobierze powiadomienie
            przypomnienie = databaseHandler.getPrzypomnienie(1, reminderId, data);
//            ReminderItem reminder = databaseHandler.getAllUsersMedicines(1);
//            ArrayList<PillItem> pillItems = reminder.getPills();
//           for(PillItem pillItem : pillItems){
//                listaLekow.add(new MediItem(pillItem.getName(), pillItem.getType(), ""));
//           }
            return przypomnienie;
        }
        catch(Exception e){
            System.out.println("Błąd przy otwieraniu bazy");
            Toast.makeText(getApplicationContext(), "Błąd pobierania danych z bazy!", Toast.LENGTH_SHORT).show();
        }
        finally {
            databaseHandler.close();
        }

        return null;
    }

    private ArrayList<MediItem> listaLekowMediItem(ArrayList<PillItem> leki) {
        ArrayList<MediItem> lekiMediItem = new ArrayList<>();

        for(PillItem lek : leki)
            lekiMediItem.add(new MediItem(lek.getUserMedicineId(), lek.getName(), lek.getType(), lek.getColor(), lek.getDosage()));

        return lekiMediItem;
    }

}
