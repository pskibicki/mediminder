package mediminder.com.mediminder.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;

import mediminder.com.mediminder.MediList;
import mediminder.com.mediminder.R;

/**
 * Created by Marcin on 2014-10-30.
 */
public class MediDeleteDialog extends DialogFragment {

    private MediList mediList;
    private int position;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.medi_delete_dialog, null))
                .setPositiveButton("Usuń", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        mediList.usunLek(position);
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });
        // Create the AlertDialog object and return it
        return builder.create();
    }

    public void setMediListAndSelectedItemPosition(MediList mediList, int position) {
        this.mediList = mediList;
        this.position = position;
    }
}