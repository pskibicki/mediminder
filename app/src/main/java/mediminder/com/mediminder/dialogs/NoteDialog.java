package mediminder.com.mediminder.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import mediminder.com.mediminder.MediAdd;
import mediminder.com.mediminder.R;
import mediminder.com.mediminder.ReminderAdd;

/**
 * Created by Marcin on 2014-10-30.
 */
public class NoteDialog extends DialogFragment {

    private MediAdd mediAdd;
    private ReminderAdd reminderAdd;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.note_dialog, null);
        builder.setView(dialogView)
                .setPositiveButton("Dodaj", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        EditText notatka = (EditText) dialogView.findViewById(R.id.noteEditText);
                        if (reminderAdd != null) {
                            reminderAdd.addNote(notatka.getText().toString());
                        }

                        if (mediAdd != null) {
                            mediAdd.dodajNotatke(notatka.getText().toString());
                        }
                    }
                })
                .setNegativeButton("Anuluj", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        // Create the AlertDialog object and return it
        return builder.create();
    }
    public void setMediAdd(MediAdd mediAdd) {
        this.mediAdd = mediAdd;
    }
    public void setReminderAdd(ReminderAdd remindAdd) {
        this.reminderAdd = remindAdd;
    }
}