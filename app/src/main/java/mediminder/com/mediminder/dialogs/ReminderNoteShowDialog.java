package mediminder.com.mediminder.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import mediminder.com.mediminder.R;

/**
 * Created by Marcin on 2014-10-30.
 */
public class ReminderNoteShowDialog extends DialogFragment {

    private static final String ARG_PARAM1 = "note";
    private String note;

    public ReminderNoteShowDialog() {

    }

    public static ReminderNoteShowDialog newInstance(String note) {
        ReminderNoteShowDialog fragment = new ReminderNoteShowDialog();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, note);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            note = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.note_show_dialog, null);
        builder.setView(dialogView)
                .setNegativeButton("Zamknij", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                    }
                });

        TextView textViewNote = (TextView) dialogView.findViewById(R.id.textViewNote);
        textViewNote.setText(note);

        // Create the AlertDialog object and return it
        return builder.create();
    }

}