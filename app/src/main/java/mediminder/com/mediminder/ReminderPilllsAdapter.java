package mediminder.com.mediminder;

import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import mediminder.com.mediminder.dialogs.ConfirmDialogFragment;
import mediminder.com.mediminder.dialogs.EndMedDialog;
import mediminder.com.mediminder.dialogs.ReminderDialogActivity;

/**
 * @author Mikołaj Antczak
 *
 * Zarzadza widokiem elementu listy przypomnienia
 */
public class ReminderPilllsAdapter extends ArrayAdapter<PillItem> {

    /**
     * Przycisk potwierdzajacy zazycie leku
     */
    private Button button;
    /**
     * Obiekt klasy obslugujacej baze
     */
    private DatabaseHandler databaseHandler;
    private LayoutInflater mInflater;
    private int layoutResource;
    /**
     * Lista lekow przypomnienia
     */
    private ArrayList<PillItem> pills;
    //widok przypomnienia w celu dostępu do przycisku potwierdzającego
    /**
     * Adapter widoku karty przypomnienia
     */
    private CardAdapter cardAdapter;
    /**
     * Nadrzedna aktywnosc - Home.java
     */
    private Activity activity;
    /**
     * Przypomnienie ktore zawiera leki
     */
    ReminderItem reminderItem;

    /**
     * Animator odpowiadajacy za animacje alpha widoku leku
     */
    private ValueAnimator alphaAnimator;
    /**
     * Animator odpowiadajacy za animacje wyszarzenia widoku leku
     */
    private ValueAnimator colorAnimator;
    /**
     * Animator zbierajacy razem aplhaAnimator i colorAnimator
     */
    private AnimatorSet animatorSet;

    /**
     * Lista jednostek lekow
     */
    private SparseArray<String> rodzajeJednostek;

    /**
     * Data aktualnej strony
     */
    private String pageDate;

    /**
     *
     * Inicjacja adapteru
     *
     * @param activity nadrzedna aktywnosc - Home.java
     * @param resource ID layoutu elementu listy
     * @param cardAdapter Nadrzedny adapter karty przypomneinia
     * @param objects Lista lekow przypomnienia
     * @param button Przycisk potwierdzania leku
     * @param item Przypomnienie
     * @param pageDate Aktualna strona
     */
    public ReminderPilllsAdapter(Activity activity, int resource, CardAdapter cardAdapter, ArrayList<PillItem> objects, Button button, ReminderItem item, String pageDate /*dla widoku tygodniowego może w */) {
        super(activity.getBaseContext(), resource, objects);

        this.cardAdapter = cardAdapter;
        pills = objects;
        this.reminderItem = item;
        this.pageDate = pageDate;
        this.activity = activity;
        this.button = button;
        layoutResource = resource;

        mInflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        rodzajeJednostek = new SparseArray<>();
        rodzajeJednostek.append(R.drawable.tab_zwykla, "szt");
        rodzajeJednostek.append(R.drawable.tab_dluga2,"szt" );
        rodzajeJednostek.append(R.drawable.tab_dzielona,"szt" );
        rodzajeJednostek.append(R.drawable.tab_dluga1,"szt" );
        rodzajeJednostek.append(R.drawable.tab_syrop,"ml");
        rodzajeJednostek.append(R.drawable.tab_strzykawka,"ml" );
        rodzajeJednostek.append(R.drawable.tab_dluga3,"szt" );
        rodzajeJednostek.append(R.drawable.tab_kropla,"szt" );
    }

    /**
     * Kontrola widoku elementu listy - ladownaie danych do widokow i specyfikacja zachowan
     *
     * @param position pozycja na liscie lekow
     * @param convertView widok elementu listy lekow
     * @param parent widok listy lekow
     * @return przetworony widok elementu listy lekow
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent){

        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(layoutResource, null);
            holder.pillLayout = (RelativeLayout) convertView.findViewById(R.id.pill_item);
            holder.pillImageView = (ImageView) convertView.findViewById(R.id.pill_item_imageview);
            holder.nameTextView = (TextView) convertView.findViewById(R.id.pill_item_textview);
            holder.doasageTextView = (TextView) convertView.findViewById(R.id.dosage);
            holder.unitTextView = (TextView) convertView.findViewById(R.id.unitTextView);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.pill_item_checkbox);

            button.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    ConfirmDialogFragment cF = new ConfirmDialogFragment();
                    cF.setParentAdapter(ReminderPilllsAdapter.this);
                    cF.show(activity.getFragmentManager(), "confirmation");
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CheckBox checkBox = (CheckBox) v.findViewById(R.id.pill_item_checkbox);
                    checkBox.setChecked(!checkBox.isChecked());
                    ((PillItem) checkBox.getTag()).setSelected(checkBox.isChecked());
                    //uaktywnia przycisk potwierdzający leki
                    updateConfirmButton(button);
                }
            });
            holder.checkBox.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ((PillItem) v.getTag()).setSelected(((CheckBox) v).isChecked());
                    updateConfirmButton(button);
                }
            });

            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        PillItem currentItem = getItem(position);

        holder.nameTextView.setText(currentItem.getName());
        holder.doasageTextView.setText(String.valueOf(currentItem.getDosage()));
        holder.unitTextView.setText(rodzajeJednostek.get(currentItem.getType()));
        holder.checkBox.setTag(currentItem);

        holder.pillImageView.setImageDrawable(getContext().getResources().getDrawable(currentItem.getType()));

        //zaznacza checkbox
        holder.checkBox.setChecked(currentItem.isSelected());
        //ustawia potwierdzenie na leku, ktory był wczesniej potwierdzony
        if(currentItem.isConfirmed())
            setPillItemConfirmed(convertView, holder, currentItem);
        else{
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.cardview_light_background));
            convertView.setAlpha(1.0f);
        }

        return convertView;
    }


    /**
     * ustawia potwierdzenie na leku, wyszarzenie go.
     *
     * @param convertView zmieniany widok
     * @param holder obiekt klasy trzymajacej aktualny stan podwidokow
     * @param currentItem aktualny element listy lekow
     */
    private void setPillItemConfirmed(View convertView, ViewHolder holder, PillItem currentItem) {
        //wyszarza potwierdzony lek
        if(currentItem.isAfterAnimation()) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.card_disabled));
            convertView.setAlpha(0.5f);
        } else {
            changeAlphaAnimation(convertView);
            currentItem.setAfterAnimation(true);
        }
        //uniemożliwia klikanie leku
        holder.checkBox.setEnabled(false);
        convertView.setEnabled(false);
        button.setEnabled(false);

    }

    /**
     * Zmienia stan przycisku aktywny/nieaktywny
     *
     * @param button Przycisk potwierdzania leku
     */
    private void updateConfirmButton(Button button) {
        boolean isToConfirm = checkIfIsAnyCheckBoxToConfirm();
        button.setEnabled(isToConfirm);
    }

    /**
     * Sprawdza czy wszystkie elementy są wybrane, jeśli tak, wyszarza przypomnienie i inicjuje usuwanie nadchodzacacego alarmu o zazyciu lekow
     * Jesli koncza sie leki inicjuje wyswietlenie sie okna dialogowego
     */
    public void confirmOnDialogAction(){
        if (checkAllCheckBoxes()) {
            reminderItem.setAfterAnimation(false);
            cardAdapter.notifyDataSetChanged();
            cancelAlarm(reminderItem);
        }

        StringBuilder endMeds = new StringBuilder();

        for (PillItem item : pills) {
            if (item.isSelected() && !item.isConfirmed()) {
                item.setConfirmed(true);
                item.setAfterAnimation(false);
                endMeds.append("\n").append(item.getName());
            }
        }

        addEndMedAlarm(endMeds.toString());

        updateReminderInDB();

        notifyDataSetChanged();

    }

    /**
     * Wyswietla okno dialogowe jesli koncza sie leki
     *
     * @param dialogText
     */
    private void addEndMedAlarm(String dialogText) {

//            Intent endIntent = new Intent(activity.getApplicationContext(), EndMedActivity.class);
//            endIntent.putExtra("names", dialogText);
//            activity.startActivity(endIntent);
        EndMedDialog newFragment = new EndMedDialog();
        Bundle bundle = new Bundle();
        bundle.putString("names", dialogText);
        newFragment.setArguments(bundle);
        newFragment.show(activity.getFragmentManager(), "delete");
    }

    /**
     * Usuwa alarm przypomnienia o zazyciu lekow
     *
     * @param reminderItem
     */
    private void cancelAlarm(ReminderItem reminderItem) {
        Intent intent = new Intent(activity.getApplicationContext(), ReminderDialogActivity.class);
        intent.putExtra("userReminderId", reminderItem.getReminderId());
        PendingIntent alarmIntent = PendingIntent.getActivity(activity.getApplicationContext(), reminderItem.getReminderId(), intent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmMgr = (AlarmManager) activity.getSystemService(Context.ALARM_SERVICE);
        alarmMgr.cancel(alarmIntent);
    }

    /**
     * Uaktoalnia przypomnienie w bazie. Uzywane po potwierdzeniu zazycia leku
     *
     * @return true jesli poprawnie zaktualizowano przypomnienie w bazie, w przeciwnym razie false
     */
    private boolean updateReminderInDB() {
        try {
            databaseHandler = new DatabaseHandler(getContext());
            databaseHandler.open();
            return databaseHandler.editReminder(reminderItem.getReminderId(), reminderItem.getPills(), reminderItem.getDateStart(), reminderItem.getDateEnd(), reminderItem.getTime(), reminderItem.getNote(), pageDate);
        } catch (Exception e) {
            //wyświetlanie komunikatu o błędzie łądowania
        } finally {
            databaseHandler.close();
        }

        return false;
    }

    /**
     * Wykonuje animacje alhpa na wiokdu elementu listy lekow przypomnienia
     *
     * @param convertView akutalnie zmieniany widok
     */
    private void changeAlphaAnimation(final View convertView) {
        animatorSet = new AnimatorSet();
        animatorSet.setDuration(300);
        animatorSet.setInterpolator(new AccelerateDecelerateInterpolator());

        Integer colorFrom = getContext().getResources().getColor(R.color.cardview_light_background);
        Integer colorTo = getContext().getResources().getColor(R.color.card_disabled);
        colorAnimator = ValueAnimator.ofObject(new ArgbEvaluator(), colorFrom, colorTo);
        colorAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                convertView.setBackgroundColor((Integer) animator.getAnimatedValue());
            }

        });
        alphaAnimator = ValueAnimator.ofFloat(1.0f, 0.5f);
        alphaAnimator.setDuration(300);
        alphaAnimator.setInterpolator(new AccelerateDecelerateInterpolator());
        alphaAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            public void onAnimationUpdate(ValueAnimator animator) {
                Float value = (Float) animator.getAnimatedValue();
                convertView.setAlpha(value.floatValue());
            }
        });

        animatorSet.playTogether(colorAnimator, alphaAnimator);
        animatorSet.start();
    }


    /**
     * Sprawdza czy jakiś element niepotwierdzony jest zaznaczony
     * @return true - jeśli CheckBox zaznaczony, w przyciwnym razie false
     */
    public boolean checkIfIsAnyCheckBoxToConfirm() {

        for(PillItem item : pills){
            if(item.isSelected() && !item.isConfirmed())
                return true;
        }

        return false;
    }

    /**
     *
     * @return true jeśli wszystie elementy listy lekow sa zaznaczone
     */
    public boolean checkAllCheckBoxes() {

        for(PillItem item : pills){
            if(!item.isSelected())
                return false;
        }

        return true;
    }
    /**
     * Trzyma informacje o aktualnym stanie widoków elementu listy lekow.
     */
    public static class ViewHolder {
        public RelativeLayout pillLayout;
        public ImageView pillImageView;
        public TextView nameTextView;
        public TextView doasageTextView;
        public TextView unitTextView;
        public CheckBox checkBox;
    }

}