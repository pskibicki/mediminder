package mediminder.com.mediminder;

import java.util.ArrayList;

/**
 * Created by Mikołaj on 2014-10-07.
 */
public class ReminderItem{

    private int reminderId;
    private String dateStart;
    private String dateEnd;
    private String time;
    private ArrayList<PillItem> pills;
    private String note;
    private boolean expanded = false;
    private int expandedHeight = 0;
    private boolean afterAnimation = true;
    private CardAdapter.ViewHolder holder;

    public ReminderItem(int reminderId, String dateStart, String dateEnd, String time, ArrayList<PillItem> pills, String note){
        super();
        this.reminderId = reminderId;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.time = time;
        this.pills = pills;
        this.note = note;
    }

    public int getReminderId() {
        return reminderId;
    }

    public void setReminderId(int reminderId) {
        this.reminderId = reminderId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public ArrayList<PillItem> getPills() {
        return pills;
    }

    public void setPills(ArrayList<PillItem> pills) {
        this.pills = pills;
    }

    public String getDateStart() {
        return dateStart;
    }

    public void setDateStart(String dateStart) {
        this.dateStart = dateStart;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public boolean isExpanded() {
        return expanded;
    }

    public void setExpanded(boolean expanded) {
        this.expanded = expanded;
    }

    public int getExpandedHeight() {
        return expandedHeight;
    }

    public void setExpandedHeight(int expandedHeight) {
        this.expandedHeight = expandedHeight;
    }

    public boolean isAfterAnimation() {
        return afterAnimation;
    }

    public void setAfterAnimation(boolean afterAnimation) {
        this.afterAnimation = afterAnimation;
    }

    public CardAdapter.ViewHolder getHolder() {
        return holder;
    }

    public void setHolder(CardAdapter.ViewHolder holder) {
        this.holder = holder;
    }
}
